import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'generator/serializable_generator.dart';

Builder builder(BuilderOptions options) =>
    PartBuilder([const SerializableGenerator()]);
