import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'enum_class.dart';
import 'serializable_class.dart';

class SerializableGenerator extends Generator {
  const SerializableGenerator();

  @override
  String generate(LibraryReader library, BuildStep buildStep) {
    final result = StringBuffer()
      ..writeln('// ignore_for_file: '
          'annotate_overrides, unnecessary_const, unnecessary_new\n')
      ..writeln(SerializableClass.generateAbstractBuilder());

    for (final element in library.allElements) {
      if (element is ClassElement) {
        if (SerializableClass.isSerializableClass(element)) {
          result.writeln(SerializableClass(element).generate());
        } else if (EnumClass.isSerializableEnumClass(element)) {
          result.writeln(EnumClass(element).generate());
        }
      }
    }

    return result.toString();
  }
}
