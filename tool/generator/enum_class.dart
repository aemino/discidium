import 'package:analyzer/dart/constant/value.dart';
import 'package:analyzer/dart/element/element.dart';
import 'package:code_builder/code_builder.dart';
import 'package:dart_style/dart_style.dart';
import 'package:discidium/src/_generator.dart' as lib;
import 'package:source_gen/source_gen.dart';

class EnumClass {
  static bool isSerializableEnumClass(ClassElement classElement) =>
      _$SerializableEnum.hasAnnotationOfExact(classElement);

  static const _$SerializableEnum =
      TypeChecker.fromRuntime(lib.SerializableEnum);

  Reference get _$Enum => Reference(enumElement.name);

  static String toMapName(String enumName) => '_to$enumName';
  static String fromMapName(String enumName) => '_from$enumName';

  final ClassElement enumElement;
  final DartObject enumAnnotation;
  final Iterable<FieldElement> enumConstants;

  EnumClass(this.enumElement)
      : enumAnnotation = _computeEnumAnnotation(enumElement),
        enumConstants = _computeEnumConstants(enumElement);

  static DartObject _computeEnumAnnotation(ClassElement element) =>
      _$SerializableEnum.firstAnnotationOfExact(element);
  static Iterable<FieldElement> _computeEnumConstants(ClassElement element) =>
      element.fields.where((field) => field.isEnumConstant);

  String generate() {
    final library = Library((libraryBuilder) => libraryBuilder.body
      ..addAll([_generateValueToEnumMap(), _generateValueFromEnumMap()]));
    return DartFormatter().format(library.accept(DartEmitter()).toString());
  }

  Object _computeValue(DartObject object) {
    switch (object.type.name) {
      case 'int':
        return object.toIntValue();
      case 'double':
        return object.toDoubleValue();
      case 'String':
        return object.toStringValue();
      default:
        throw UnimplementedError();
    }
  }

  Iterable<Expression> _generateKeysIterable() =>
      enumConstants.map((value) => _$Enum.property(value.name));

  Iterable<Object> _generateValuesIterable() =>
      enumAnnotation.getField('values').toListValue().map(_computeValue);

  Code _generateValueToEnumMap() => literalConstMap(
          Map.fromIterables(_generateValuesIterable(), _generateKeysIterable()))
      .assignConst(toMapName(_$Enum.symbol))
      .statement;

  Code _generateValueFromEnumMap() => literalConstMap(
          Map.fromIterables(_generateKeysIterable(), _generateValuesIterable()))
      .assignConst(fromMapName(_$Enum.symbol))
      .statement;
}
