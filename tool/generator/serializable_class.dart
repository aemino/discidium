import 'package:analyzer/dart/constant/value.dart';
import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/type.dart';
import 'package:code_builder/code_builder.dart';
import 'package:dart_style/dart_style.dart';
import 'package:discidium/src/_generator.dart' as lib;
import 'package:source_gen/source_gen.dart';

import 'enum_class.dart';

// TODO: Create consts for References (?)

class AnnotatedAccessorElement {
  final PropertyAccessorElement element;
  final DartObject annotation;

  String get serializedName =>
      ConstantReader(annotation).read('serializedName').stringValue;

  String get deserializedName => element.name;

  AnnotatedAccessorElement(this.element, this.annotation);
}

class SerializableClass {
  static bool isSerializableClass(ClassElement classElement) =>
      _$SerializableType.hasAnnotationOfExact(classElement) ||
      (classElement.supertype != null &&
          _$ResourceType.isSuperOf(classElement));

  static bool isAbstractResourceClass(ClassElement classElement) =>
      classElement.interfaces
          .any((interface) => _$ResourceType.isSuperOf(interface.element));

  final ClassElement serializableElement;
  final bool isAbstract;

  final Iterable<AnnotatedAccessorElement> _allFields;

  static const _$SerializableType = TypeChecker.fromRuntime(lib.Serializable);
  static const _$SnowflakeType = TypeChecker.fromRuntime(lib.Snowflake);
  static const _$ResourceType = TypeChecker.fromRuntime(lib.Resource);
  static const _$FieldType = TypeChecker.fromRuntime(lib.Field);

  static Reference get _$BaseBuilder => Reference(_builderClassName('Base'));

  static const _$Client = Reference('Client');
  static const _$Json = Reference('Json');

  static String _serializableClassName(String name) => '_\$$name';
  static String _builderClassName(String name) => '_\$${name}Builder';

  Reference get _$SerializableClass =>
      Reference(_serializableClassName(serializableElement.name));

  Reference get _$BuilderClass =>
      Reference(_builderClassName(serializableElement.name));

  SerializableClass(this.serializableElement, {this.isAbstract = false})
      : _allFields = _computeAllFields(serializableElement);

  String generate() {
    final library = Library((libraryBuilder) => libraryBuilder.body
      ..addAll([_generateBuilderClass(), _generateSerializableClass()]));
    return DartFormatter().format(library.accept(DartEmitter()).toString());
  }

  static String generateAbstractBuilder() {
    final library = Library((libraryBuilder) =>
        libraryBuilder.body..addAll([_generateAbstractBuilderClass()]));
    return DartFormatter().format(library.accept(DartEmitter()).toString());
  }

  static Class _generateAbstractBuilderClass() =>
      Class((classBuilder) => classBuilder
        ..abstract = true
        ..name = _$BaseBuilder.symbol
        ..methods.addAll([
          Method((methodBuilder) => methodBuilder
            ..name = 'toJson'
            ..returns = _$Json)
        ]));

  static Iterable<AnnotatedAccessorElement> _computeAllFields(
      ClassElement element) {
    final fieldAccumulator = <String, PropertyAccessorElement>{};
    final interfaceAccumulator = Set<InterfaceType>();

    void recurseInterface(InterfaceType type) {
      if (!interfaceAccumulator.add(type)) return;

      if (type.superclass != null) {
        recurseInterface(type.superclass);
      }

      type.mixins.forEach(recurseInterface);

      fieldAccumulator.addEntries(
          type.accessors.map((accessor) => MapEntry(accessor.name, accessor)));
    }

    recurseInterface(element.type);

    return fieldAccumulator.values.where(_$FieldType.hasAnnotationOfExact).map(
        (accessor) => AnnotatedAccessorElement(
            accessor, _$FieldType.firstAnnotationOfExact(accessor)));
  }

  String _generateDeserializer(AnnotatedAccessorElement field) {
    final name = field.serializedName;

    final value = 'data[\'$name\']';
    final unwrapped = '$value.unwrap()';

    var nullSafety = true;
    var expression = 'null';

    final returnType = field.element.returnType;

    switch (returnType.name) {
      case 'DateTime':
        expression = 'DateTime.parse($unwrapped)';
        break;
      case 'Json':
        expression = value;
        nullSafety = false;
        break;
      case 'List':
        nullSafety = false;
        expression = 'List.from($value.unwrap())';
        break;
      default:
        if (EnumClass.isSerializableEnumClass(returnType.element)) {
          expression = '${EnumClass.toMapName(returnType.name)}[$unwrapped]';
        } else {
          nullSafety = false;
          expression = unwrapped;
        }
    }

    final optional = field.annotation.getField('optional').toBoolValue();
    final nullable = field.annotation.getField('nullable').toBoolValue();

    if (nullable && nullSafety) {
      expression = '($unwrapped == null ? null : $expression)';
    }

    return optional
        ? 'data.containsKey(\'$name\') ? $expression : builder.${field
            .deserializedName}'
        : expression;
  }

  String _generateSerializer(AnnotatedAccessorElement field) {
    final name = field.deserializedName;

    final value = name;

    var expression = 'null';

    final returnType = field.element.returnType;

    switch (returnType.name) {
      case 'DateTime':
        expression = '$value?.toIso8601String()';
        break;
      default:
        if (EnumClass.isSerializableEnumClass(returnType.element)) {
          expression = '${EnumClass.fromMapName(returnType.name)}[$value]';
        } else {
          expression = value;
        }
    }

    return expression;
  }

  Iterable<Field> _generateFields(
          Iterable<AnnotatedAccessorElement> fields, FieldModifier modifier) =>
      fields.map((field) => Field((fieldBuilder) => fieldBuilder
        ..name = field.deserializedName
        ..type = Reference(
            _$SnowflakeType.isSuperOf(field.element.returnType.element)
                ? field.element.returnType.name
                : field.element.returnType.displayName)
        ..modifier = modifier));

  Class _generateBuilderClass() => Class((classBuilder) => classBuilder
    ..name = _builderClassName(serializableElement.name)
    ..implements.addAll([
      serializableElement.supertype != null &&
              isSerializableClass(serializableElement.supertype.element)
          ? Reference(_builderClassName(serializableElement.supertype.name))
          : _$BaseBuilder
    ])
    ..fields.addAll(_generateFields(_allFields, FieldModifier.var$))
    ..methods.addAll([
      Method((methodBuilder) => methodBuilder
        ..name = 'patch'
        ..lambda = true
        ..static = true
        ..requiredParameters.addAll([
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'builder'
            ..type = _$BuilderClass),
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'data'
            ..type = _$Json),
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'client'
            ..type = _$Client)
        ])
        ..returns = _$BuilderClass
        ..body = Code.scope((_) =>
            'builder\n' +
            _allFields
                .map((field) => '..${field
                    .deserializedName} = ${_generateDeserializer(field)}')
                .join('\n'))),
      Method((methodBuilder) => methodBuilder
        ..name = 'fromJson'
        ..lambda = true
        ..static = true
        ..requiredParameters.addAll(([
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'data'
            ..type = _$Json),
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'client'
            ..type = _$Client)
        ]))
        ..returns = _$BuilderClass
        ..body = _$BuilderClass.property('patch').call([
          _$BuilderClass.call([]),
          const Reference('data'),
          const Reference('client')
        ]).code),
      Method((methodBuilder) => methodBuilder
        ..name = 'build'
        ..lambda = true
        ..static = true
        ..requiredParameters.addAll([
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'builder'
            ..type = _$BuilderClass),
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'client'
            ..type = _$Client)
        ])
        ..returns = _$SerializableClass
        ..body = _$SerializableClass.newInstanceNamed('_fromBuilder',
            [const Reference('client'), const Reference('builder')]).code),
      Method((methodBuilder) => methodBuilder
        ..name = 'toJson'
        ..returns = _$Json
        ..body = Block.of([
          _$Json.newInstance([literalMap({})]).assignFinal('json').statement
        ]
            .followedBy(_allFields.map((field) => Code.scope((_) =>
                'if (${field.deserializedName} != null) json[\'${field
                    .serializedName}\'] = '
                '${_generateSerializer(field)};')))
            .followedBy([const Reference('json').returned.statement])))
    ]));

  Class _generateSerializableClass() => Class((classBuilder) => classBuilder
    ..name = _serializableClassName(serializableElement.name)
    ..extend = Reference(serializableElement.displayName)
    ..mixins.addAll(
        serializableElement.mixins.map((mixin) => Reference(mixin.displayName)))
    ..implements.addAll(serializableElement.interfaces
        .map((interface) => Reference(interface.displayName)))
    ..fields.addAll(_generateFields(_allFields, FieldModifier.final$))
    ..constructors.addAll([
      Constructor((constructorBuilder) => constructorBuilder
        ..lambda = true
        ..factory = true
        ..requiredParameters.addAll([
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'client'
            ..type = _$Client),
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'data'
            ..type = _$Json)
        ])
        ..body = _$BuilderClass.property('build').call([
          _$BuilderClass
              .property('fromJson')
              .call([const Reference('data'), const Reference('client')]),
          const Reference('client')
        ]).code),
      Constructor((constructorBuilder) => constructorBuilder
        ..name = '_fromBuilder'
        ..requiredParameters.addAll([
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'client'
            ..type = _$Client),
          Parameter((parameterBuilder) => parameterBuilder
            ..name = 'builder'
            ..type = _$BuilderClass)
        ])
        ..initializers.addAll(_allFields
            .map((field) => Reference(field.deserializedName)
                .assign(
                    const Reference('builder').property(field.deserializedName))
                .code)
            .followedBy([
          const Reference('super')
              .property('_internal')
              .call([const Reference('client')]).code
        ])))
    ]));
}
