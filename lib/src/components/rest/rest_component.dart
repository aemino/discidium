import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../client.dart';
import '../../network/rest.dart';
import '../../util/constants.dart';
import '../../util/json.dart';
import 'route_builder.dart';

const protocolVersions = {'api': 7, 'gateway': 7};
final endpoints = {
  'api': 'https://discordapp.com/api/v${protocolVersions['api']}/'
};
final defaultUserAgent =
    'DiscordBot (${package['name']}, ${package['version']}) ${platform['os']} '
    'Dart/${platform['version']}';

class RestComponent {
  final Client client;
  final ApiClient restClient;

  final Uri apiUrl;

  RestComponent(this.client, {String apiEndpoint, String userAgent})
      : apiUrl = Uri.parse(apiEndpoint ?? endpoints['api']),
        restClient =
            ApiClient('Bot ${client.token}', userAgent ?? defaultUserAgent);

  Route get api => Route(this, apiUrl);

  Future<Json> request(String method, Route route,
      [Json data, Iterable<http.MultipartFile> files]) async {
    final encodedData = data == null ? null : json.encode(data.unwrap());

    final request = files == null
        ? restClient.request(method, route.url, body: encodedData)
        : restClient.multipart(method, route.url,
            fields: {'payload_json': encodedData}, files: files);

    final response = await request;

    RangeError.checkValueInInterval(response.statusCode, 200, 299);

    final jsonObject = await response
        .read()
        .transform(utf8.decoder)
        .transform(json.decoder)
        .first;

    return Json(jsonObject);
  }
}
