import 'dart:async';

import 'package:http/http.dart';

import '../../util/json.dart';
import 'rest_component.dart';

class Route {
  final RestComponent _rest;

  final Uri url;

  Route(this._rest, this.url);

  Future<Json> get() => _rest.request('get', this);
  Future<Json> post([Json data, Iterable<MultipartFile> files]) =>
      _rest.request('post', this, data, files);
  Future<Json> put([Json data, Iterable<MultipartFile> files]) =>
      _rest.request('put', this, data, files);
  Future<Json> patch([Json data, Iterable<MultipartFile> files]) =>
      _rest.request('patch', this, data, files);
  Future<Json> delete() => _rest.request('delete', this);

  Route operator +(String name) => Route(
      _rest, url.replace(pathSegments: List.of(url.pathSegments)..add(name)));
}
