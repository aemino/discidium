import 'dart:async';

import '../event_component.dart';
import '../events/index.dart';
import '../gateway.dart';
import '../packet.dart';

class DispatchAcceptor {
  final EventPipeline pipeline;

  DispatchAcceptor(this.pipeline);

  Future<void> onPacket(Packet packet) async {
    if (packet.op != Opcode.dispatch) return;

    final eventHandler = eventHandlers[packet.name];
    if (eventHandler != null) {
      await eventHandler(pipeline.client, pipeline, packet.data);
    }
  }
}
