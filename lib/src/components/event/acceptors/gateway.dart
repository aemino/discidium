import 'dart:async';
import 'dart:math';

import '../../../util/constants.dart';
import '../../../util/json.dart';
import '../event_component.dart';
import '../events/index.dart';
import '../gateway.dart';
import '../packet.dart';

class GatewayAcceptor {
  static const int identifyDelayMs = 5500;

  final EventPipeline pipeline;

  int _sequence;
  int _heartbeatInterval;

  Timer _heartbeat;

  DateTime lastHeartbeatTimestamp;
  DateTime lastHeartbeatAckTimestamp;

  String sessionID;

  int identifyAttempts = 0;

  GatewayAcceptor(this.pipeline) {
    pipeline.gateway?.onClose?.listen(onClose);
  }

  int get sequence => _sequence;
  set sequence(int seq) => _sequence = (seq > (sequence ?? 0) ? seq : sequence);

  int get heartbeatInterval => _heartbeatInterval;

  set heartbeatInterval(int interval) {
    if (interval == heartbeatInterval) return;

    _heartbeat?.cancel();

    if (interval == null) {
      _heartbeatInterval = null;
      _heartbeat = null;
      pipeline.client.onDebug.add('Cleared heartbeat timer');
      return;
    }

    _heartbeat = Timer.periodic(Duration(milliseconds: interval), heartbeat);

    pipeline.client.onDebug.add('Created heartbeat timer ($interval ms)');
  }

  /// Compute the delay before re-identifying should be attempted based on the
  /// number of successive identify attempts.
  Duration computeIdentifyDelay() {
    if (identifyAttempts < 1) return const Duration(milliseconds: 0);

    // TODO: Use alternative if shard count and/or shard ID are known.

    return Duration(
        milliseconds:
            (Random().nextInt(identifyAttempts) + 1) * identifyDelayMs);
  }

  Future<void> onPacket(Packet packet) async {
    if (packet.sequence != null) sequence = packet.sequence;

    switch (packet.op) {
      case Opcode.hello:
        await hello(packet);
        break;
      case Opcode.heartbeat:
        await heartbeat();
        break;
      case Opcode.heartbeatAck:
        await heartbeatAck();
        break;
      case Opcode.invalidSession:
        await invalidSession(packet);
        break;
      case Opcode.dispatch:
        // Dispatch events which are pertinent to the gateway are handled here.
        switch (packet.name) {
          case DispatchEvent.ready:
            // Although the dispatch acceptor handles this event, it is also handled
            // here to capture the session ID in case the dispatch acceptor is not
            // being used.
            await ready(packet);
            break;
          case DispatchEvent.resumed:
            await resumed(packet);
            break;
          default:
            break;
        }

        break;
      default:
        break;
    }
  }

  Future<void> onClose(int code) async {
    try {
      switch (code) {
        case WebSocketStatus.normalClosure:
          cleanup(full: true);
          return;
        case GatewayStatus.authenticationFailed:
          throw ArgumentError('Invalid token provided.');
        case GatewayStatus.invalidShard:
          throw ArgumentError('Invalid shard specified.');
        case GatewayStatus.shardingRequired:
          throw ArgumentError('Sharding is required.');
        default:
          break;
      }
    } catch (err) {
      cleanup(full: true);
      rethrow;
    }

    cleanup();
    await pipeline.gateway?.connect();
  }

  void cleanup({bool full = false}) {
    heartbeatInterval = null;

    lastHeartbeatTimestamp = null;
    lastHeartbeatAckTimestamp = null;

    if (full) {
      _sequence = null;
      sessionID = null;
    }
  }

  Future<void> identify() async {
    final packet = Packet(
        op: Opcode.identify,
        data: Json({
          'token': pipeline.client.token,
          'properties': {
            '\$os': platform['os'],
            '\$browser': package['name'],
            '\$device': package['name']
          },
          'compress': pipeline.config.inputCompressionScheme ==
              PayloadCompressionScheme.zlib
        }));

    pipeline.gateway?.state = GatewayState.identifying;
    identifyAttempts++;
    await pipeline.onOutgoingPacket.add(packet);
  }

  Future<void> resume() async {
    if (sessionID == null) {
      throw StateError('Attempted to resume, but no session ID was saved.');
    }

    final packet = Packet(
        op: Opcode.resume,
        data: Json({
          'token': pipeline.client.token,
          'session_id': sessionID,
          'seq': sequence
        }));

    pipeline.gateway?.state = GatewayState.resuming;
    await pipeline.onOutgoingPacket.add(packet);
  }

  Future<void> hello(Packet packet) async {
    pipeline.client.onDebug.add('Received hello event.');

    final data = packet.data;
    heartbeatInterval = data['heartbeat_interval'].unwrap();

    // If a session ID is stored, attempt to resume.
    if (sessionID == null) {
      pipeline.client.onDebug.add(
          'No prior session ID stored; proceeding with standard identify.');
      await identify();
    } else {
      pipeline.client.onDebug
          .add('Prior session ID found; attempting to resume.');
      await resume();
    }
  }

  Future<void> heartbeat([Timer timer]) async {
    if (lastHeartbeatTimestamp != null) {
      if (lastHeartbeatAckTimestamp == null ||
          lastHeartbeatTimestamp.isAfter(lastHeartbeatAckTimestamp)) {
        await heartbeatAckMissed();
        return;
      }
    }

    pipeline.client.onDebug.add('Sending heartbeat.');

    final packet = Packet(op: Opcode.heartbeat, data: Json(sequence));
    lastHeartbeatTimestamp = DateTime.now();
    await pipeline.onOutgoingPacket.add(packet);
  }

  Future<void> heartbeatAck() async {
    if (lastHeartbeatTimestamp == null) {
      pipeline.client.onDebug
          .add('Received a hearbeat ack without sending a hearbeat');
      return;
    }

    lastHeartbeatAckTimestamp = DateTime.now();

    final int latency = lastHeartbeatAckTimestamp
        .difference(lastHeartbeatTimestamp)
        .inMilliseconds;
    pipeline.client.onDebug
        .add('Received a heartbeat ack with latency of $latency ms');
  }

  Future<void> invalidSession(Packet packet) async {
    final isResumable = packet.data.unwrap();

    pipeline.client.onDebug
        .add('Session was invalidated and ${isResumable ? 'can' : 'cannot'} be '
            'resumed.');

    if (isResumable) {
      await resume();
      return;
    }

    pipeline.gateway?.state = GatewayState.connected;

    final reidentifyDelay = computeIdentifyDelay();

    pipeline.client.onDebug.add('Performing full cleanup and re-identifying in '
        '${reidentifyDelay.inSeconds} seconds.');

    // Because the session is no longer resumable, we need to cleanup and
    // dispose of the old session ID.
    cleanup(full: true);

    // The documentation indicates that the delay before re-identifying should
    // be a random number between 1 and 5 seconds.
    await Future.delayed(reidentifyDelay, identify);
  }

  /// This method is called when a heartbeat ack is not sent between heartbeats.
  Future<void> heartbeatAckMissed() async {
    _heartbeat?.cancel();

    pipeline.client.onDebug
        .add('Gateway didn\'t send a heartbeat ack in time.');

    await pipeline.gateway?.close(1002);
  }

  Future<void> ready(Packet packet) async {
    sessionID = packet.data['session_id'].unwrap();
    pipeline.gateway?.state = GatewayState.ready;
    identifyAttempts = 0;
  }

  Future<void> resumed(Packet packet) async {
    pipeline.client.onDebug.add('Successfully resumed gateway connection.');
    pipeline.gateway?.state = GatewayState.ready;
  }
}
