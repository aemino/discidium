import 'dart:async';
import 'dart:math';

import '../../client.dart';
import '../../network/ws.dart';
import '../../util/json.dart';
import '../rest/rest_component.dart';
import 'event_component.dart';

export 'dart:io' show WebSocketStatus;

enum GatewayState {
  disconnected,
  connecting,
  connected,
  identifying,
  resuming,
  ready
}

abstract class Opcode {
  static const int dispatch = 0;
  static const int heartbeat = 1;
  static const int identify = 2;
  static const int statusUpdate = 3;
  static const int voiceServerUpdate = 4;
  static const int voiceServerPing = 5;
  static const int resume = 6;
  static const int reconnect = 7;
  static const int requestGuildMembers = 8;
  static const int invalidSession = 9;
  static const int hello = 10;
  static const int heartbeatAck = 11;
}

abstract class GatewayStatus {
  static const int unknownError = 4000;
  static const int unknownOpcode = 4001;
  static const int decodeError = 4002;
  static const int notAuthenticated = 4003;
  static const int authenticationFailed = 4004;
  static const int alreadyAuthenticated = 4005;
  static const int invalidSequence = 4007;
  static const int rateLimited = 4008;
  static const int sessionTimeout = 4009;
  static const int invalidShard = 4010;
  static const int shardingRequired = 4011;
}

class GatewayConnection extends WebSocket {
  final EventPipeline pipeline;

  GatewayState _state = GatewayState.disconnected;

  Client get client => pipeline.client;

  GatewayConnection(this.pipeline) : super() {
    onError.listen(_onError);
    onClose.listen(_onClose);
  }

  Future<void> connect(
      {int maxAttempts = 25,
      Duration timeout = const Duration(seconds: 5)}) async {
    for (int attempt = 1; attempt <= maxAttempts; attempt++) {
      try {
        await _initiateConnection().timeout(timeout);
        state = GatewayState.connected;
        return;
      } catch (err) {
        client.onDebug.add(err.toString());

        state = GatewayState.disconnected;

        final delay = Duration(seconds: (pow(2, attempt.clamp(1, 4)) * 5));
        client.onDebug.add('Failed to connect to the gateway. '
            'Retrying in ${delay.inSeconds} seconds...');

        await Future.delayed(delay);
      }
    }

    // TODO: Throw an error and clean up client.

    client.onDebug.add('Reached maximum of $maxAttempts connection attempts. '
        'Ceasing attempts to connect.');
  }

  Future<void> _initiateConnection() async {
    final Json gateway = await (client.api + 'gateway').get();

    final payloadCodec = pipeline.config.inputPayloadCodec;
    final payloadCompression = pipeline.config.inputCompressionScheme;

    final queryParameters = {
      'v': protocolVersions['gateway'].toString(),
      'encoding': pipeline.config.encodingCodecName(payloadCodec)
    };

    if (pipeline.config.compressionSchemeIsTransport(payloadCompression)) {
      queryParameters['compress'] =
          pipeline.config.compressionSchemeName(payloadCompression);
    }

    final gatewayUrl = Uri.parse(gateway['url'].unwrap())
        .replace(queryParameters: queryParameters);

    client.onDebug.add('Gateway url: $gatewayUrl');
    state = GatewayState.connecting;
    await connectSocket(gatewayUrl);
  }

  GatewayState get state => _state;
  set state(GatewayState newState) {
    client.onDebug.add('Gateway connection state updating: ${_state
        .toString()} -> ${newState.toString()}');

    _state = newState;
  }

  void _onError(dynamic error) {
    client.onDebug.add('Gateway connection encountered an error:\n'
        '$error');
  }

  void _onClose(int code) {
    state = GatewayState.disconnected;

    client.onDebug.add('Gateway connection closed with code $code:\n'
        '${socket.closeReason}');
  }
}
