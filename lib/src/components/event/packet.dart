import '../../util/json.dart';

class Packet {
  int op;
  Json data;
  int sequence;
  String name;

  Packet({this.op, this.data, this.sequence, this.name});
  Packet.fromJson(Json data)
      : this(
            op: data['op'].unwrap(),
            data: data['d'],
            sequence: data['s'].unwrap(),
            name: data['t'].unwrap());

  Json toJson() =>
      Json({'op': op, 'd': data.unwrap(), 's': sequence, 't': name});
}
