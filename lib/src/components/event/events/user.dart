part of 'event.dart';

// TODO: Events which contain users should store them.
class UserUpdateEvent extends Event {
  final Json _user;
  final Json _formerUser;

  /// The updated user that was received.
  User get user => client.users.instantiate(_user);

  /// The former user instance before this event occurred.
  User get formerUser => client.users.instantiate(_formerUser);

  UserUpdateEvent(EventPipeline pipeline, this._user, this._formerUser)
      : super(pipeline);

  static Future<UserUpdateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.users.update(data);
    if (!result.success) return null;

    final user = result.after;
    final formerUser = result.before;

    final event = UserUpdateEvent(pipeline, user, formerUser);
    if (artificial) return event;

    client.onUserUpdate.add(event);
    return event;
  }
}
