import 'dart:async';

import '../../../client.dart';
import '../../../util/json.dart';
import '../event_component.dart';
import 'event.dart';

abstract class DispatchEvent {
  static const String ready = 'READY';
  static const String resumed = 'RESUMED';
  static const String channelCreate = 'CHANNEL_CREATE';
  static const String channelUpdate = 'CHANNEL_UPDATE';
  static const String channelDelete = 'CHANNEL_DELETE';
  static const String channelPinsUpdate = 'CHANNEL_PINS_UPDATE';
  static const String channelTypingStart = 'TYPING_START';
  static const String guildCreate = 'GUILD_CREATE';
  static const String guildUpdate = 'GUILD_UPDATE';
  static const String guildDelete = 'GUILD_DELETE';
  static const String guildBanAdd = 'GUILD_BAN_ADD';
  static const String guildBanRemove = 'GUILD_BAN_REMOVE';
  static const String guildEmojisUpdate = 'GUILD_EMOJIS_UPDATE';
  static const String guildIntegrationsUpdate = 'GUILD_INTEGRATIONS_UPDATE';
  static const String memberCreate = 'GUILD_MEMBER_ADD';
  static const String memberUpdate = 'GUILD_MEMBER_UPDATE';
  static const String memberDelete = 'GUILD_MEMBER_REMOVE';
  static const String memberChunk = 'GUILD_MEMBERS_CHUNK';
  static const String roleCreate = 'GUILD_ROLE_CREATE';
  static const String roleUpdate = 'GUILD_ROLE_UPDATE';
  static const String roleDelete = 'GUILD_ROLE_DELETE';
  static const String messageCreate = 'MESSAGE_CREATE';
  static const String messageUpdate = 'MESSAGE_UPDATE';
  static const String messageDelete = 'MESSAGE_DELETE';
  static const String messageDeleteBulk = 'MESSAGE_DELETE_BULK';
  static const String messageReactionAdd = 'MESSAGE_REACTION_ADD';
  static const String messageReactionRemove = 'MESSAGE_REACTION_REMOVE';
  static const String messageReactionRemoveAll = 'MESSAGE_REACTION_REMOVE_ALL';
  static const String presenceUpdate = 'PRESENCE_UPDATE';
  static const String userUpdate = 'USER_UPDATE';
  static const String voiceStateUpdate = 'VOICE_STATE_UPDATE';
  static const String voiceServerUpdate = 'VOICE_SERVER_UPDATE';
  static const String webhooksUpdate = 'WEBHOOKS_UPDATE';
}

// TODO: Consider dropping the requirement that event handlers return an Event.
typedef EventHandler = Future<Event> Function(
    Client client, EventPipeline pipeline, Json data,
    {bool artificial});

const eventHandlers = <String, EventHandler>{
  DispatchEvent.ready: ReadyEvent.handle,
  DispatchEvent.channelCreate: ChannelCreateEvent.handle,
  DispatchEvent.channelUpdate: ChannelUpdateEvent.handle,
  DispatchEvent.channelDelete: ChannelDeleteEvent.handle,
  DispatchEvent.channelPinsUpdate: ChannelPinsUpdateEvent.handle,
  DispatchEvent.channelTypingStart: ChannelTypingStartEvent.handle,
  DispatchEvent.guildCreate: GuildCreateEvent.handle,
  DispatchEvent.guildUpdate: GuildUpdateEvent.handle,
  DispatchEvent.guildDelete: GuildDeleteEvent.handle,
  DispatchEvent.guildBanAdd: GuildBanAddEvent.handle,
  DispatchEvent.guildBanRemove: GuildBanRemoveEvent.handle,
  DispatchEvent.messageCreate: MessageCreateEvent.handle,
  DispatchEvent.messageUpdate: MessageUpdateEvent.handle,
  DispatchEvent.presenceUpdate: PresenceUpdateEvent.handle,
  DispatchEvent.roleCreate: RoleCreateEvent.handle,
  DispatchEvent.roleUpdate: RoleUpdateEvent.handle,
  DispatchEvent.roleDelete: RoleDeleteEvent.handle,
  DispatchEvent.userUpdate: UserUpdateEvent.handle
};
