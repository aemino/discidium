part of 'event.dart';

class ReadyEvent extends Event {
  ReadyEvent(EventPipeline pipeline) : super(pipeline);

  static Future<ReadyEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    await client.guilds.createMany(data['guilds'].asList());
    await client.channels.createMany(data['private_channels'].asList());

    await client.users.create(data['user']);

    final event = ReadyEvent(pipeline);
    client.onReady.add(event);

    return event;
  }
}
