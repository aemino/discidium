part of 'event.dart';

class MessageCreateEvent extends Event {
  final Json _message;

  /// The new message that was received.
  Message get message => client.messages.instantiate(_message);

  MessageCreateEvent(EventPipeline pipeline, this._message) : super(pipeline);

  static Future<MessageCreateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.messages.create(data);
    final message = result.after;

    final event = MessageCreateEvent(pipeline, message);
    if (artificial) return event;

    client.onMessageCreate.add(event);
    return event;
  }
}

class MessageUpdateEvent extends Event {
  final Json _message;
  final Json _formerMessage;

  /// The updated message that was received.
  Message get message => client.messages.instantiate(_message);

  /// The former message instance before this event occurred.
  Message get formerMessage => client.messages.instantiate(_formerMessage);

  MessageUpdateEvent(EventPipeline pipeline, this._message, this._formerMessage)
      : super(pipeline);

  static Future<MessageUpdateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.messages.update(data);
    if (!result.success) return null;

    final message = result.after;
    final formerMessage = result.before;

    final event = MessageUpdateEvent(pipeline, message, formerMessage);
    if (artificial) return event;

    client.onMessageUpdate.add(event);
    return event;
  }
}

class MessageDeleteEvent extends Event {
  final Json _message;

  /// The former message instance before this event occurred.
  Message get message => client.messages.instantiate(_message);

  MessageDeleteEvent(EventPipeline pipeline, this._message) : super(pipeline);

  static Future<MessageDeleteEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.messages.delete(data);
    if (!result.success) return null;

    final event = MessageDeleteEvent(pipeline, data);
    if (artificial) return event;

    client.onMessageDelete.add(event);
    return event;
  }
}
