library discord.events;

import 'dart:async';

import '../../../client.dart';
import '../../../resources/resource.dart';
import '../../../structures/structure.dart';
import '../../../util/json.dart';
import '../event_component.dart';

part 'channel.dart';
part 'guild.dart';
part 'member.dart';
part 'message.dart';
part 'presence.dart';
part 'ready.dart';
part 'role.dart';
part 'user.dart';

abstract class Event {
  /// The event pipeline that produced this event.
  final EventPipeline pipeline;

  /// The client related to this event's pipeline.
  Client get client => pipeline.client;

  Event(this.pipeline);
}
