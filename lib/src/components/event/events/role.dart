part of 'event.dart';

class RoleCreateEvent extends Event {
  final String _guildID;
  final Json _role;

  /// The guild in which this event occurred.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  /// The new role that was created.
  Role get role => client.roles.instantiate(_role);

  RoleCreateEvent(EventPipeline pipeline, this._guildID, this._role)
      : super(pipeline);

  static Future<RoleCreateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    data.unwrap().addAll(data.remove('role').unwrap());

    final result = await client.roles.create(data);
    final role = result.after;

    final String id = data['id'].unwrap();
    final String guildID = data['guild_id'].unwrap();

    final guild = await client.guilds.get(guildID);

    final List<String> roles = guild['roles'].unwrap()..add(id);
    await client.guilds.update(Json({'id': guildID, 'roles': roles}));

    final event = RoleCreateEvent(pipeline, guildID, role);
    if (artificial) return event;

    client.onRoleCreate.add(event);
    return event;
  }
}

class RoleUpdateEvent extends Event {
  final String _guildID;
  final Json _role;
  final Json _formerRole;

  /// The guild in which this event occurred.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  /// The updated role that was received.
  Role get role => client.roles.instantiate(_role);

  /// The former role instance before this event occurred.
  Role get formerRole => client.roles.instantiate(_formerRole);

  RoleUpdateEvent(
      EventPipeline pipeline, this._guildID, this._role, this._formerRole)
      : super(pipeline);

  static Future<RoleUpdateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final String guildID = data['guild_id'].unwrap();
    data.unwrap().addAll(data.remove('role').unwrap());

    final result = await client.roles.update(data);
    if (!result.success) return null;

    final role = result.after;
    final formerRole = result.before;

    final event = RoleUpdateEvent(pipeline, guildID, role, formerRole);
    if (artificial) return event;

    client.onRoleUpdate.add(event);
    return event;
  }
}

class RoleDeleteEvent extends Event {
  final String _guildID;
  final Json _role;

  /// The guild in which this event occurred.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  /// The former role instance before this event occurred.
  Role get role => client.roles.instantiate(_role);

  RoleDeleteEvent(EventPipeline pipeline, this._guildID, this._role)
      : super(pipeline);

  static Future<RoleDeleteEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    data['id'] = data.remove('role_id').unwrap();

    final result = await client.roles.delete(data);
    final role = result.before;

    final String id = data['id'].unwrap();
    final String guildID = data['guild_id'].unwrap();

    final guild = await client.guilds.get(guildID);

    final List<String> roles = guild['roles'].unwrap()..remove(id);
    await client.guilds.update(Json({'id': guildID, 'roles': roles}));

    final event = RoleDeleteEvent(pipeline, guildID, role);
    if (artificial) return event;

    client.onRoleDelete.add(event);
    return event;
  }
}
