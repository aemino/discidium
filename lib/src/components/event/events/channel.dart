part of 'event.dart';

class ChannelCreateEvent extends Event {
  final Json _channel;

  /// The new channel that was created.
  Channel get message => client.channels.instantiate(_channel);

  ChannelCreateEvent(EventPipeline pipeline, this._channel) : super(pipeline);

  static Future<ChannelCreateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.channels.create(data);
    final channel = result.after;

    final String id = data['id'].unwrap();
    final String guildID = data['guild_id'].unwrap();

    if (guildID != null) {
      final guild = await client.guilds.get(guildID);

      final List<String> channels = guild['channels'].unwrap()..add(id);
      await client.guilds.update(Json({'id': guildID, 'channels': channels}));
    }

    final event = ChannelCreateEvent(pipeline, channel);
    if (artificial) return event;

    client.onChannelCreate.add(event);
    return event;
  }
}

class ChannelUpdateEvent extends Event {
  final Json _channel;
  final Json _formerChannel;

  /// The updated channel that was received.
  Channel get channel => client.channels.instantiate(_channel);

  /// The former channel instance before this event occurred.
  Channel get formerChannel => client.channels.instantiate(_formerChannel);

  ChannelUpdateEvent(EventPipeline pipeline, this._channel, this._formerChannel)
      : super(pipeline);

  static Future<ChannelUpdateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.channels.update(data);
    if (!result.success) return null;

    final channel = result.after;
    final formerChannel = result.before;

    final event = ChannelUpdateEvent(pipeline, channel, formerChannel);
    if (artificial) return event;

    client.onChannelUpdate.add(event);
    return event;
  }
}

class ChannelDeleteEvent extends Event {
  final Json _channel;

  /// The former channel instance before this event occurred.
  Channel get channel => client.channels.instantiate(_channel);

  ChannelDeleteEvent(EventPipeline pipeline, this._channel) : super(pipeline);

  static Future<ChannelDeleteEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.channels.delete(data);
    if (!result.success) return null;

    final channel = client.channels.transform(data);

    final String id = data['id'].unwrap();
    final String guildID = data['guild_id'].unwrap();

    if (guildID != null) {
      final guild = await client.guilds.get(guildID);
      final List<String> channels = guild['channels'].unwrap()..remove(id);

      await client.guilds.update(Json({'id': guildID, 'channels': channels}));
    }

    final event = ChannelDeleteEvent(pipeline, channel);
    if (artificial) return event;

    client.onChannelDelete.add(event);
    return event;
  }
}

// This class doesn't extend Event because it is a delegating event.
class ChannelPinsUpdateEvent {
  static Future<ChannelUpdateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) {
    data['id'] = data['channel_id'];
    data.remove('channel_id');

    return ChannelUpdateEvent.handle(client, pipeline, data,
        artificial: artificial);
  }
}

class ChannelTypingStartEvent extends Event {
  final String _channelID;
  final String _userID;
  final int _timestamp;

  /// The channel in which this event occurred.
  MessageChannelSnowflake get channel =>
      MessageChannelSnowflake(client, _channelID);

  /// The user who began typing.
  UserSnowflake get user => UserSnowflake(client, _userID);

  /// The time at which this event started
  DateTime get startedAt =>
      DateTime.fromMillisecondsSinceEpoch(_timestamp * 1000, isUtc: true);

  ChannelTypingStartEvent(
      EventPipeline pipeline, this._channelID, this._userID, this._timestamp)
      : super(pipeline);

  static Future<ChannelTypingStartEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final event = ChannelTypingStartEvent(pipeline, data['channel_id'].unwrap(),
        data['user_id'].unwrap(), data['timestamp'].unwrap());

    if (artificial) return event;

    client.onChannelTypingStart.add(event);
    return event;
  }
}
