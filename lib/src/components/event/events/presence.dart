part of 'event.dart';

class PresenceUpdateEvent extends Event {
  final String _guildID;
  final Json _presence;

  /// The guild that this event occurred in.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  /// The updated presence that was received.
  Presence get presence => Presence(client, _presence);

  // TODO: Implement former presence.

  PresenceUpdateEvent(EventPipeline pipeline, this._guildID, this._presence)
      : super(pipeline);

  static Future<PresenceUpdateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final String guildID = data['guild_id'].unwrap();

    // TODO: Store presence updates.

    final event = PresenceUpdateEvent(pipeline, guildID, data);
    if (artificial) return event;

    client.onPresenceUpdate.add(event);
    return event;
  }
}
