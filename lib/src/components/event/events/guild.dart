part of 'event.dart';

class GuildCreateEvent extends Event {
  final Json _guild;

  /// The relevant guild instance.
  Guild get guild => client.guilds.instantiate(_guild);

  GuildCreateEvent(EventPipeline pipeline, this._guild) : super(pipeline);

  static Future<GuildCreateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {artificial = false}) async {
    final String id = data['id'].unwrap();

    data['roles'].asList().forEach((role) => role['guild_id'] = id);
    data['channels'].asList().forEach((channel) => channel['guild_id'] = id);
    data['members'].asList().forEach((role) => role['guild_id'] = id);

    await client.channels.createMany(data['channels'].asList());
    await client.roles.createMany(data['roles'].asList());
    // await client.members.createMany(data['members'].asList());

    final existingGuild = await client.guilds.get(id);

    final result = await client.guilds.create(data);
    final guild = result.after;

    final event = GuildCreateEvent(pipeline, guild);
    if (artificial) return event;

    final bool guildWasUnavailable = existingGuild['unavailable'].unwrap();

    if (existingGuild != null) {
      client.onGuildJoin.add(event);
    } else if (guildWasUnavailable) {
      client.onGuildAvailable.add(event);
    } else {
      return null;
    }

    return event;
  }
}

class GuildUpdateEvent extends Event {
  final Json _guild;
  final Json _formerGuild;

  /// The updated guild that was received.
  Guild get guild => client.guilds.instantiate(_guild);

  /// The former guild instance before this event occurred.
  Guild get formerGuild => client.guilds.instantiate(_formerGuild);

  GuildUpdateEvent(EventPipeline pipeline, this._guild, this._formerGuild)
      : super(pipeline);

  static Future<GuildUpdateEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final result = await client.guilds.update(data);
    if (!result.success) return null;

    final guild = result.after;
    final formerGuild = result.before;

    final event = GuildUpdateEvent(pipeline, guild, formerGuild);
    if (artificial) return event;

    client.onGuildUpdate.add(event);
    return event;
  }
}

class GuildDeleteEvent extends Event {
  final Json _guild;

  /// The former guild instance before this event occurred.
  Guild get guild => client.guilds.instantiate(_guild);

  GuildDeleteEvent(EventPipeline pipeline, this._guild) : super(pipeline);

  static Future<GuildDeleteEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final becameUnavailable = data['unavailable'] != null;

    final event = GuildDeleteEvent(pipeline, data);

    if (becameUnavailable) {
      await client.guilds.update(data);
    } else {
      final result = await client.guilds.delete(data);
      if (!result.success) return null;
    }

    if (artificial) return event;

    if (becameUnavailable) {
      client.onGuildUnavailable.add(event);
    } else {
      client.onGuildLeave.add(event);
    }

    return event;
  }
}

class GuildBanAddEvent extends Event {
  final String _guildID;
  final Json _user;

  /// The guild in which this event occurred.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  /// The user whose ban was removed.
  User get user => client.users.instantiate(_user);

  GuildBanAddEvent(EventPipeline pipeline, this._guildID, this._user)
      : super(pipeline);

  static Future<GuildBanAddEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final String guildID = data.remove('guild_id').unwrap();

    final event = GuildBanAddEvent(pipeline, guildID, data);
    if (artificial) return event;

    client.onGuildUserBan.add(event);
    return event;
  }
}

class GuildBanRemoveEvent extends Event {
  final String _guildID;
  final Json _user;

  /// The guild in which this event occurred.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  /// The user whose ban was removed.
  User get user => client.users.instantiate(_user);

  GuildBanRemoveEvent(EventPipeline pipeline, this._guildID, this._user)
      : super(pipeline);

  static Future<GuildBanRemoveEvent> handle(
      Client client, EventPipeline pipeline, Json data,
      {bool artificial = false}) async {
    final String guildID = data.remove('guild_id').unwrap();

    final event = GuildBanRemoveEvent(pipeline, guildID, data);
    if (artificial) return event;

    client.onGuildUserUnban.add(event);
    return event;
  }
}
