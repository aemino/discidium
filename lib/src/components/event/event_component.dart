import 'dart:async';
import 'dart:convert';

import '../../client.dart';
import '../../util/events.dart';
import '../../util/json.dart';
import 'acceptors/dispatch.dart';
import 'acceptors/gateway.dart';
import 'gateway.dart';
import 'packet.dart';

enum PayloadEncodingCodec { json, etf }

enum PayloadCompressionScheme { none, zlib, zlibStream }

class EventPipelineOptions {
  static const Map<PayloadEncodingCodec, String> _encodingCodecToName = {
    PayloadEncodingCodec.json: 'json',
    PayloadEncodingCodec.etf: 'etf'
  };

  static const Map<PayloadCompressionScheme, String> _compressionSchemeToName =
      {
    PayloadCompressionScheme.none: 'none',
    PayloadCompressionScheme.zlib: 'zlib',
    PayloadCompressionScheme.zlibStream: 'zlib-stream'
  };

  static const List<PayloadCompressionScheme> _transportCompressionSchemes = [
    PayloadCompressionScheme.zlibStream
  ];

  final PayloadEncodingCodec inputPayloadCodec;
  final PayloadCompressionScheme inputCompressionScheme;

  final PayloadEncodingCodec outputPayloadCodec;
  final PayloadCompressionScheme outputCompressionScheme;

  final bool createGatewayConnection;
  final bool handleDispatchEvents;

  // TODO: Improve pipeline config defaults.
  // Default input + output codec to 'etf' and input compression to
  // 'zlibStream' where applicable.
  const EventPipelineOptions(
      {this.inputPayloadCodec = PayloadEncodingCodec.json,
      this.inputCompressionScheme = PayloadCompressionScheme.none,
      this.outputPayloadCodec = PayloadEncodingCodec.json,
      this.outputCompressionScheme = PayloadCompressionScheme.none,
      this.createGatewayConnection = true,
      this.handleDispatchEvents = true});

  bool compressionSchemeIsTransport(PayloadCompressionScheme scheme) =>
      _transportCompressionSchemes.contains(scheme);

  String encodingCodecName(PayloadEncodingCodec codec) =>
      _encodingCodecToName[codec];

  String compressionSchemeName(PayloadCompressionScheme scheme) =>
      _compressionSchemeToName[scheme];
}

class InputPayloadTransformer extends StreamTransformerBase<dynamic, Packet> {
  final EventPipelineOptions config;

  InputPayloadTransformer(this.config);

  @override
  Stream<Packet> bind(Stream<dynamic> stream) => stream.map(convert);

  Packet convert(dynamic payload) {
    switch (config.inputCompressionScheme) {
      case PayloadCompressionScheme.none:
        break;
      default:
        throw UnimplementedError();
    }

    switch (config.inputPayloadCodec) {
      case PayloadEncodingCodec.json:
        final packetData = Json(json.decode(payload));
        return Packet.fromJson(packetData);
      default:
        throw UnimplementedError();
    }
  }
}

class OutputPayloadTransformer extends StreamTransformerBase<Packet, dynamic> {
  final EventPipelineOptions config;

  OutputPayloadTransformer(this.config);

  @override
  Stream<dynamic> bind(Stream<Packet> stream) => stream.map(convert);

  dynamic convert(Packet packet) {
    var payload;

    switch (config.inputPayloadCodec) {
      case PayloadEncodingCodec.json:
        payload = json.encode(packet.toJson().unwrap());
        break;
      default:
        throw UnimplementedError();
    }

    switch (config.inputCompressionScheme) {
      case PayloadCompressionScheme.none:
        return payload;
      default:
        throw UnimplementedError();
    }
  }
}

// TODO: Create an onDebug EventStream for EventPipeline.
// The client's onDebug should then transition to being an aggregator for
// each pipeline's debug logs.
/// A transparent pipeline for events to flow through.
class EventPipeline {
  final EventComponent events;
  final EventPipelineOptions config;

  final InputPayloadTransformer inputTransformer;
  final OutputPayloadTransformer outputTransformer;

  final EventStream<dynamic> onIncomingPayload = EventStream();
  final EventStream<Packet> onIncomingPacket = EventStream();
  final EventStream<Packet> onOutgoingPacket = EventStream();
  final EventStream<dynamic> onOutgoingPayload = EventStream();

  GatewayConnection gateway;

  DispatchAcceptor dispatchAcceptor;
  GatewayAcceptor gatewayAcceptor;

  Client get client => events.client;

  EventPipeline(this.events, this.config)
      : inputTransformer = InputPayloadTransformer(config),
        outputTransformer = OutputPayloadTransformer(config) {
    onIncomingPayload.transform(inputTransformer).listen(onIncomingPacket.add);
    onOutgoingPacket.transform(outputTransformer).listen(onOutgoingPayload.add);

    if (config.handleDispatchEvents) {
      dispatchAcceptor = DispatchAcceptor(this);
      onIncomingPacket.listen(dispatchAcceptor.onPacket);
    }

    if (config.createGatewayConnection) {
      gateway = GatewayConnection(this);
      gatewayAcceptor = GatewayAcceptor(this);
      onIncomingPacket.listen(gatewayAcceptor.onPacket);
    }
  }

  Future<void> destroy() async {
    await destroyEvents();
  }

  Future<void> destroyEvents() => Future.wait([
        onIncomingPayload.close(),
        onIncomingPacket.close(),
        onOutgoingPacket.close(),
        onOutgoingPayload.close()
      ]);
}

/// A router for raw event and packet data.
class EventComponent {
  final Client client;
  final List<EventPipeline> pipelines = [];

  EventComponent(this.client);

  EventPipeline createPipeline(EventPipelineOptions config) {
    final pipeline = EventPipeline(this, config);
    pipelines.add(pipeline);
    return pipeline;
  }

  Future<void> destroy() =>
      Future.wait(pipelines.map((pipeline) => pipeline.destroy()));
}
