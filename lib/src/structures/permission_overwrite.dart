part of 'structure.dart';

@Serializable()
abstract class PermissionOverwrite {
  final Client client;

  factory PermissionOverwrite(Client client, Json data) = _$PermissionOverwrite;

  PermissionOverwrite._internal(this.client);

  @Field('id')
  String get _id;

  /// The snowflake identifier of the resource this overwrite pertains to.
  Snowflake get id {
    switch (type) {
      case PermissionOverwriteType.role:
        return RoleSnowflake(client, _id, null);
      case PermissionOverwriteType.member:
        return MemberSnowflake(client, _id, null);
      default:
        throw UnimplementedError();
    }
  }

  @Field('type')
  PermissionOverwriteType get type;

  @Field('allow')
  int get _allow;

  @Field('deny')
  int get _deny;

  Permissions get allow => Permissions(_allow);

  Permissions get deny => Permissions(_deny);
}

@SerializableEnum(['role', 'member'])
enum PermissionOverwriteType { role, member }
