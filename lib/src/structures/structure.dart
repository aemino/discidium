library discidium.structures;

import '../client.dart';
import '../resources/resource.dart';
import '../util/json.dart';
import '../util/meta.dart';

part 'permission_overwrite.dart';
part 'permissions.dart';
part 'presence.dart';
part 'structure.g.dart';
