part of 'structure.dart';

/// A representation of Discord permissions.
///
/// Provides advanced manipulation in the form of operators.
///
/// Example of using permissions:
///
///     // Create a permission object that contains the permissions to kick, ban, and
///     // move members.
///     final moderatorPermissions =
///         Permissions.kickMembers | Permissions.banMembers | Permissions.moveMembers;
///
///     // Check if the member's permissions are a superset of the moderator
///     // permissions. In other words, check if the member's permissions contain all
///     // of the moderator permissions.
///     final isModerator = (member.permissions >= moderatorPermissions);
///
class Permissions {
  final int bitfield;

  const Permissions([this.bitfield = 0]);

  /// Computes the intersection between this permissions object and [other].
  Permissions operator &(Permissions other) =>
      Permissions(bitfield & other.bitfield);

  /// Computes the union of this permissions object and [other].
  Permissions operator |(Permissions other) =>
      Permissions(bitfield | other.bitfield);

  /// Computes the union of this permissions object minus the intersection of
  /// [other].
  Permissions operator ^(Permissions other) =>
      Permissions(bitfield ^ other.bitfield);

  /// Computes the inverse of this permissions object.
  Permissions operator ~() => Permissions(~bitfield);

  /// True if this permissions object is a superset of [other].
  bool operator >=(Permissions other) => (this | other) == this;

  /// True if this permissions object is a subset of [other].
  bool operator <=(Permissions other) => (this & other) == this;

  /// True if this permissions object is a proper superset of [other].
  bool operator >(Permissions other) => (this | other) != other;

  /// True if this permissions object is a proper subset of [other].
  bool operator <(Permissions other) => (this & other) != other;

  @override
  bool operator ==(other) => hashCode == other.hashCode;

  @override
  int get hashCode => bitfield;

  static final createInvites = const Permissions(1 << 0);
  static final kickMembers = const Permissions(1 << 1);
  static final banMembers = const Permissions(1 << 2);
  static final administrator = const Permissions(1 << 3);
  static final manageChannels = const Permissions(1 << 4);
  static final manageGuild = const Permissions(1 << 5);
  static final addReactions = const Permissions(1 << 6);
  static final viewAuditLog = const Permissions(1 << 7);

  static final viewChannel = const Permissions(1 << 10);
  static final sendMessages = const Permissions(1 << 11);
  static final sendTtsMessages = const Permissions(1 << 12);
  static final manageMessages = const Permissions(1 << 13);
  static final embedLinks = const Permissions(1 << 14);
  static final attachFiles = const Permissions(1 << 15);
  static final readMessageHistory = const Permissions(1 << 16);
  static final mentionEveryone = const Permissions(1 << 17);
  static final useExternalEmojis = const Permissions(1 << 18);

  static final connect = const Permissions(1 << 20);
  static final speak = const Permissions(1 << 21);
  static final muteMembers = const Permissions(1 << 22);
  static final deafenMembers = const Permissions(1 << 23);
  static final moveMembers = const Permissions(1 << 24);
  static final useVad = const Permissions(1 << 25);

  static final changeNickname = const Permissions(1 << 26);
  static final manageNicknames = const Permissions(1 << 27);
  static final manageRoles = const Permissions(1 << 28);
  static final manageWebhooks = const Permissions(1 << 29);
  static final manageEmojis = const Permissions(1 << 30);

  static final all = createInvites |
      kickMembers |
      banMembers |
      administrator |
      manageChannels |
      manageGuild |
      addReactions |
      viewAuditLog |
      viewChannel |
      sendMessages |
      sendTtsMessages |
      manageMessages |
      embedLinks |
      attachFiles |
      readMessageHistory |
      mentionEveryone |
      useExternalEmojis |
      connect |
      speak |
      deafenMembers |
      moveMembers |
      useVad |
      changeNickname |
      manageNicknames |
      manageRoles |
      manageWebhooks |
      manageEmojis;
}
