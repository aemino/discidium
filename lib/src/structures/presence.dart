part of 'structure.dart';

@Serializable()
abstract class Presence {
  final Client client;

  factory Presence(Client client, Json data) = _$Presence;

  Presence._internal(this.client);

  @Field('user')
  Json get _user;

  /// The user that this presence represents.
  UserSnowflake get user => UserSnowflake(client, _user['id'].unwrap());

  /// The member that this presence represents.
  UserSnowflake get member =>
      MemberSnowflake(client, _user['id'].unwrap(), _guildID);

  @Field('game', nullable: true)
  Json get _activity;

  /// The activity of this presence.
  PresenceActivity get activity => null;

  @Field('guild_id')
  String get _guildID;

  /// The guild that this presence was created for.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  /// The status of this presence.
  @Field('status', optional: true)
  PresenceStatus get status;
}

@SerializableEnum(['idle', 'dnd', 'online', 'offline'])
enum PresenceStatus { idle, doNotDisturb, online, offline }

@Serializable()
abstract class PresenceActivity {
  final Client client;

  factory PresenceActivity(Client client, Json data) = _$PresenceActivity;

  PresenceActivity._internal(this.client);

  @Field('name')
  String get name;

  @Field('type')
  PresenceActivityType get type;

  @Field('url', optional: true, nullable: true)
  String get _url;

  /// A relevant URL for this activity, if applicable.
  Uri get url => _url == null ? null : Uri.parse(_url);

  @Field('timestamps', optional: true)
  Json get _timestampsRaw;

  _PresenceActivityTimestamps get _timestamps => null;

  /// The time at which this activity started, if applicable.
  DateTime get startedAt => _timestamps?.start == null
      ? null
      : DateTime.fromMillisecondsSinceEpoch(_timestamps.start, isUtc: true);

  /// The time at which this activity ended, if applicable.
  DateTime get endedAt => _timestamps?.end == null
      ? null
      : DateTime.fromMillisecondsSinceEpoch(_timestamps.end, isUtc: true);

  // TODO: application

  /// A string describing arbitrary information about this activity, if
  /// applicable.
  @Field('details', optional: true, nullable: true)
  String get details;

  /// A string describing the state of this activity, if applicable.
  @Field('state', optional: true, nullable: true)
  String get state;

  // TODO: party

  // TODO: assets
}

@SerializableEnum([0, 1, 2])
enum PresenceActivityType { game, stream, audio }

@Serializable()
abstract class _PresenceActivityTimestamps {
  final Client client;

  factory _PresenceActivityTimestamps(Client client, Json data) = _$_PresenceActivityTimestamps;

  _PresenceActivityTimestamps._internal(this.client);

  @Field('start', optional: true)
  int get start;

  @Field('end', optional: true)
  int get end;
}
