// GENERATED CODE - DO NOT MODIFY BY HAND

part of discidium.structures;

// **************************************************************************
// SerializableGenerator
// **************************************************************************

// ignore_for_file: annotate_overrides, unnecessary_const, unnecessary_new

abstract class _$BaseBuilder {
  Json toJson();
}

class _$PermissionOverwriteBuilder implements _$BaseBuilder {
  String _id;

  PermissionOverwriteType type;

  int _allow;

  int _deny;

  static _$PermissionOverwriteBuilder patch(
          _$PermissionOverwriteBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toPermissionOverwriteType[data['type'].unwrap()]
        .._allow = data['allow'].unwrap()
        .._deny = data['deny'].unwrap();
  static _$PermissionOverwriteBuilder fromJson(Json data, Client client) =>
      _$PermissionOverwriteBuilder.patch(
          _$PermissionOverwriteBuilder(), data, client);
  static _$PermissionOverwrite build(
          _$PermissionOverwriteBuilder builder, Client client) =>
      new _$PermissionOverwrite._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromPermissionOverwriteType[type];
    if (_allow != null) json['allow'] = _allow;
    if (_deny != null) json['deny'] = _deny;
    return json;
  }
}

class _$PermissionOverwrite extends PermissionOverwrite {
  factory _$PermissionOverwrite(Client client, Json data) =>
      _$PermissionOverwriteBuilder.build(
          _$PermissionOverwriteBuilder.fromJson(data, client), client);

  _$PermissionOverwrite._fromBuilder(
      Client client, _$PermissionOverwriteBuilder builder)
      : _id = builder._id,
        type = builder.type,
        _allow = builder._allow,
        _deny = builder._deny,
        super._internal(client);

  final String _id;

  final PermissionOverwriteType type;

  final int _allow;

  final int _deny;
}

const _toPermissionOverwriteType = const {
  'role': PermissionOverwriteType.role,
  'member': PermissionOverwriteType.member
};
const _fromPermissionOverwriteType = const {
  PermissionOverwriteType.role: 'role',
  PermissionOverwriteType.member: 'member'
};

class _$PresenceBuilder implements _$BaseBuilder {
  Json _user;

  Json _activity;

  String _guildID;

  PresenceStatus status;

  static _$PresenceBuilder patch(
          _$PresenceBuilder builder, Json data, Client client) =>
      builder
        .._user = data['user']
        .._activity = data['game']
        .._guildID = data['guild_id'].unwrap()
        ..status = data.containsKey('status')
            ? _toPresenceStatus[data['status'].unwrap()]
            : builder.status;
  static _$PresenceBuilder fromJson(Json data, Client client) =>
      _$PresenceBuilder.patch(_$PresenceBuilder(), data, client);
  static _$Presence build(_$PresenceBuilder builder, Client client) =>
      new _$Presence._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_user != null) json['user'] = _user;
    if (_activity != null) json['game'] = _activity;
    if (_guildID != null) json['guild_id'] = _guildID;
    if (status != null) json['status'] = _fromPresenceStatus[status];
    return json;
  }
}

class _$Presence extends Presence {
  factory _$Presence(Client client, Json data) =>
      _$PresenceBuilder.build(_$PresenceBuilder.fromJson(data, client), client);

  _$Presence._fromBuilder(Client client, _$PresenceBuilder builder)
      : _user = builder._user,
        _activity = builder._activity,
        _guildID = builder._guildID,
        status = builder.status,
        super._internal(client);

  final Json _user;

  final Json _activity;

  final String _guildID;

  final PresenceStatus status;
}

const _toPresenceStatus = const {
  'idle': PresenceStatus.idle,
  'dnd': PresenceStatus.doNotDisturb,
  'online': PresenceStatus.online,
  'offline': PresenceStatus.offline
};
const _fromPresenceStatus = const {
  PresenceStatus.idle: 'idle',
  PresenceStatus.doNotDisturb: 'dnd',
  PresenceStatus.online: 'online',
  PresenceStatus.offline: 'offline'
};

class _$PresenceActivityBuilder implements _$BaseBuilder {
  String name;

  PresenceActivityType type;

  String _url;

  Json _timestampsRaw;

  String details;

  String state;

  static _$PresenceActivityBuilder patch(
          _$PresenceActivityBuilder builder, Json data, Client client) =>
      builder
        ..name = data['name'].unwrap()
        ..type = _toPresenceActivityType[data['type'].unwrap()]
        .._url = data.containsKey('url') ? data['url'].unwrap() : builder._url
        .._timestampsRaw = data.containsKey('timestamps')
            ? data['timestamps']
            : builder._timestampsRaw
        ..details = data.containsKey('details')
            ? data['details'].unwrap()
            : builder.details
        ..state =
            data.containsKey('state') ? data['state'].unwrap() : builder.state;
  static _$PresenceActivityBuilder fromJson(Json data, Client client) =>
      _$PresenceActivityBuilder.patch(
          _$PresenceActivityBuilder(), data, client);
  static _$PresenceActivity build(
          _$PresenceActivityBuilder builder, Client client) =>
      new _$PresenceActivity._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (name != null) json['name'] = name;
    if (type != null) json['type'] = _fromPresenceActivityType[type];
    if (_url != null) json['url'] = _url;
    if (_timestampsRaw != null) json['timestamps'] = _timestampsRaw;
    if (details != null) json['details'] = details;
    if (state != null) json['state'] = state;
    return json;
  }
}

class _$PresenceActivity extends PresenceActivity {
  factory _$PresenceActivity(Client client, Json data) =>
      _$PresenceActivityBuilder.build(
          _$PresenceActivityBuilder.fromJson(data, client), client);

  _$PresenceActivity._fromBuilder(
      Client client, _$PresenceActivityBuilder builder)
      : name = builder.name,
        type = builder.type,
        _url = builder._url,
        _timestampsRaw = builder._timestampsRaw,
        details = builder.details,
        state = builder.state,
        super._internal(client);

  final String name;

  final PresenceActivityType type;

  final String _url;

  final Json _timestampsRaw;

  final String details;

  final String state;
}

const _toPresenceActivityType = const {
  0: PresenceActivityType.game,
  1: PresenceActivityType.stream,
  2: PresenceActivityType.audio
};
const _fromPresenceActivityType = const {
  PresenceActivityType.game: 0,
  PresenceActivityType.stream: 1,
  PresenceActivityType.audio: 2
};

class _$_PresenceActivityTimestampsBuilder implements _$BaseBuilder {
  int start;

  int end;

  static _$_PresenceActivityTimestampsBuilder patch(
          _$_PresenceActivityTimestampsBuilder builder,
          Json data,
          Client client) =>
      builder
        ..start =
            data.containsKey('start') ? data['start'].unwrap() : builder.start
        ..end = data.containsKey('end') ? data['end'].unwrap() : builder.end;
  static _$_PresenceActivityTimestampsBuilder fromJson(
          Json data, Client client) =>
      _$_PresenceActivityTimestampsBuilder.patch(
          _$_PresenceActivityTimestampsBuilder(), data, client);
  static _$_PresenceActivityTimestamps build(
          _$_PresenceActivityTimestampsBuilder builder, Client client) =>
      new _$_PresenceActivityTimestamps._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (start != null) json['start'] = start;
    if (end != null) json['end'] = end;
    return json;
  }
}

class _$_PresenceActivityTimestamps extends _PresenceActivityTimestamps {
  factory _$_PresenceActivityTimestamps(Client client, Json data) =>
      _$_PresenceActivityTimestampsBuilder.build(
          _$_PresenceActivityTimestampsBuilder.fromJson(data, client), client);

  _$_PresenceActivityTimestamps._fromBuilder(
      Client client, _$_PresenceActivityTimestampsBuilder builder)
      : start = builder.start,
        end = builder.end,
        super._internal(client);

  final int start;

  final int end;
}
