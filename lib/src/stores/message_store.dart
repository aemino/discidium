part of 'store.dart';

class MessageStore extends ResourceStore<MessageSnowflake, Message> {
  MessageStore(Client client, StoreSpecifier storeSpecifier)
      : super(client, storeSpecifier);

  @override
  MessageSnowflake instantiateSnowflake(Json data) => MessageSnowflake(
      client, data['id'].unwrap(), data['channel_id'].unwrap());

  @override
  Message instantiate(Json data) => Message(client, data);

  @override
  Future<Message> fetchFromApi(MessageSnowflake snowflake,
      {bool fromCache = true,
      bool fromStore = true,
      bool fromApi = true}) async {
    final data = await snowflake.route.get();
    if (data == null) return null;

    await MessageCreateEvent.handle(client, null, data, artificial: true);
    return instantiate(data);
  }

  @override
  Json transform(Json data) {
    final users = data['mentions']?.asList();

    if (users != null) {
      data['mentions'] = users.map((user) => user['id'].unwrap()).toList();
    }

    return data;
  }
}
