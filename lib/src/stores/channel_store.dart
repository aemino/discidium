part of 'store.dart';

class ChannelStore extends ResourceStore<ChannelSnowflake, Channel> {
  ChannelStore(Client client, StoreSpecifier storeSpecifier)
      : super(client, storeSpecifier);

  @override
  ChannelSnowflake instantiateSnowflake(Json data) =>
      ChannelSnowflake(client, data['id'].unwrap());

  @override
  Channel instantiate(Json data) => Channel(client, data);

  @override
  Future<Channel> fetchFromApi(ChannelSnowflake snowflake,
      {bool fromCache = true,
      bool fromStore = true,
      bool fromApi = true}) async {
    final data = await snowflake.route.get();
    if (data == null) return null;

    // await ChannelCreateEvent.handle(client, data, artificial: true);
    return instantiate(data);
  }
}
