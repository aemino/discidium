library discord.stores;

import 'dart:async';

import '../client.dart';
import '../components/event/events/event.dart';
import '../resources/resource.dart';
import '../util/json.dart';

part 'sweepable.dart';

part 'channel_store.dart';
part 'guild_store.dart';
part 'member_store.dart';
part 'message_store.dart';
part 'role_store.dart';
part 'user_store.dart';

abstract class BaseStore {
  FutureOr<bool> has(String id);
  FutureOr<int> count([Json query]);
  FutureOr<Json> get(String id);
  FutureOr<Json> set(String id, Json value);

  FutureOr<CreateResult> create(String id, Json value);
  FutureOr<UpdateResult> update(String id, Json value);
  FutureOr<DeleteResult> delete(String id);

  FutureOr<List<CreateResult>> createMany(List<String> ids, List<Json> values);
  FutureOr<List<UpdateResult>> updateMany(List<String> ids, List<Json> values);
  FutureOr<List<DeleteResult>> deleteMany(List<String> ids);
}

class MemoryStore extends Sweepable implements BaseStore {
  final Map<String, Json> _store = {};

  MemoryStore(SweepOptions options) : super(options);

  @override
  bool has(String id) => _store.containsKey(id);

  @override
  int count([Json query]) {
    if (query == null) return all().length;
    return where(query).length;
  }

  List<Json> all() => _store.values.toList();

  List<Json> where(Json query) => _store.values
      .where((v) => query.asMap().keys.every((k) => v[k] == v))
      .toList();

  @override
  Json get(String id) => _store[id]?.clone();

  @override
  Json set(String id, Json value) {
    value['stored_at'] = DateTime.now().toUtc().toIso8601String();
    return _store[id] = value?.clone();
  }

  @override
  CreateResult create(String id, Json value) {
    if (has(id)) {
      final result = update(id, value);
      return CreateResult(result.after);
    }

    set(id, value);

    return CreateResult(value);
  }

  @override
  UpdateResult update(String id, Json value) {
    final former = get(id);
    if (former == null) return null;

    final updated = former.clone();
    updated.unwrap().addAll(value.unwrap());

    _store[id] = updated;

    return UpdateResult(former, updated);
  }

  @override
  DeleteResult delete(String id) {
    final former = get(id);
    if (former == null) return DeleteResult.failure();

    _store.remove(id);
    return DeleteResult(former);
  }

  @override
  List<CreateResult> createMany(List<String> ids, List<Json> values) {
    final results = <CreateResult>[];

    for (var i = 0; i < ids.length; i++) {
      results.add(create(ids[i], values[i]));
    }

    return results;
  }

  @override
  List<UpdateResult> updateMany(List<String> ids, List<Json> values) {
    final results = <UpdateResult>[];

    for (var i = 0; i < ids.length; i++) {
      results.add(update(ids[i], values[i]));
    }

    return results;
  }

  @override
  List<DeleteResult> deleteMany(List<String> ids) {
    final results = <DeleteResult>[];

    for (var i = 0; i < ids.length; i++) {
      results.add(delete(ids[i]));
    }

    return results;
  }

  @override
  void sweep(Duration lifetime) {
    for (final id in _store.keys) {
      final instance = _store[id];
      final storedAt = DateTime.parse(instance['stored_at'].unwrap());
      final isExpired = storedAt.add(lifetime).isAfter(DateTime.now().toUtc());

      if (isExpired) _store.remove(id);
    }
  }
}

typedef StoreSpecifier = BaseStore Function();

abstract class ResourceStore<S extends Snowflake<R>, R extends Resource> {
  final BaseStore _store;
  final Client client;

  FutureOr<bool> has(String id) => _store.has(id);

  FutureOr<int> count([Json query]) => _store.count(query);

  FutureOr<Json> get(String id) => _store.get(id);

  FutureOr<Json> set(String id, Json value) => _store.set(id, value);

  ResourceStore(this.client, StoreSpecifier storeSpecifier)
      : _store = storeSpecifier();

  String keyFromJson(Json data) => data['id'].unwrap();

  Json transform(Json data) => data;

  S instantiateSnowflake(Json data);
  R instantiate(Json data);

  Future<R> fetchFromApi(S snowflake,
      {bool fromCache = true, bool fromStore = true, bool fromApi = true});

  Iterable<S> instantiateAllSnowflakes(Iterable<Json> data) =>
      data.map(instantiateSnowflake);

  Iterable<R> instantiateAll(Iterable<Json> data) => data.map(instantiate);

  Future<R> fetch(Snowflake<R> snowflake,
      {bool fromCache = true,
      bool fromStore = true,
      bool fromApi = true}) async {
    if (!(fromCache || fromStore || fromApi)) {
      throw ArgumentError('At least one retrieval source must be used.');
    }

    // TODO: implement cache here

    if (fromStore) {
      final data = await get(snowflake.toString());
      if (data != null) return instantiate(data);
    }

    if (fromApi) {
      final instance = await fetchFromApi(snowflake);
      if (instance != null) return instance;
    }

    throw StateError('Could not find requested resource.');
  }

  /// Create a resource using the provided [data].
  /// 
  /// If the resource already exists, it will be updated.
  Future<CreateResult> create(Json data) async {
    final id = keyFromJson(data);
    final transformedData = transform(data.clone());

    return await _store.create(id, transformedData);
  }

  /// Update an existing resource using the provided [data].
  Future<UpdateResult> update(Json data) async {
    final id = keyFromJson(data);
    final transformedData = transform(data.clone());

    return await _store.update(id, transformedData);
  }

  /// Delete an existing resource using the provided [data].
  Future<DeleteResult> delete(Json data) async {
    final id = keyFromJson(data);

    return await _store.delete(id);
  }

  /// Create multiple resources using the provided [data].
  /// 
  /// If the resource already exists, it will be updated.
  Future<List<CreateResult>> createMany(List<Json> data) async {
    final ids = data.map(keyFromJson).toList();
    final transformedData =
        data.map((data) => transform(data.clone())).toList();

    return await _store.createMany(ids, transformedData);
  }

  /// Update multiple resources using the provided [data].
  Future<List<UpdateResult>> updateMany(List<Json> data) async {
    final ids = data.map(keyFromJson);
    final transformedData =
        data.map((data) => transform(data.clone())).toList();

    return await _store.updateMany(ids, transformedData);
  }

  /// Delete multiple resources using the provided [data].
  Future<List<DeleteResult>> deleteMany(List<Json> data) async {
    final ids = data.map(keyFromJson);
    return await _store.deleteMany(ids);
  }
}

/// A wrapper class for the result of a [BaseStore.create] operation.
class CreateResult {
  /// The object after it was created.
  final Json after;

  CreateResult(this.after);
}

/// A wrapper class for the result of a [BaseStore.update] operation.
class UpdateResult {
  /// False if the object did not exist.
  final bool success;

  /// The object before it was updated.
  final Json before;

  /// The object after it was updated.
  final Json after;

  UpdateResult(this.before, this.after, {this.success = true});
  UpdateResult.failure()
      : success = false,
        before = null,
        after = null;
}

/// A wrapper class for the result of a [BaseStore.delete] operation.
class DeleteResult {
  /// False if the object did not exist.
  final bool success;

  /// The object before it was deleted.
  final Json before;

  DeleteResult(this.before, {this.success = true});
  DeleteResult.failure()
      : success = false,
        before = null;
}
