part of 'store.dart';

class UserStore extends ResourceStore<UserSnowflake, User> {
  UserStore(Client client, StoreSpecifier storeSpecifier)
      : super(client, storeSpecifier);

  @override
  UserSnowflake instantiateSnowflake(Json data) =>
      UserSnowflake(client, data['id'].unwrap());

  @override
  User instantiate(Json data) => User(client, data);

  @override
  Future<User> fetchFromApi(UserSnowflake snowflake,
      {bool fromCache = true,
      bool fromStore = true,
      bool fromApi = true}) async {
    final data = await snowflake.route.get();
    if (data == null) return null;

    // await UserCreateEvent.handle(client, data, artificial: true);
    return instantiate(data);
  }
}
