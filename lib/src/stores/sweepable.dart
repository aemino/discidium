part of discord.stores;

class SweepOptions {
  final Duration interval;
  final Duration lifetime;

  const SweepOptions({this.interval, this.lifetime});
}

abstract class Sweepable {
  Timer sweepTimer;
  SweepOptions sweepOptions;

  Sweepable(this.sweepOptions) {
    if (sweepOptions.interval == null || sweepOptions.lifetime == null) return;

    sweepTimer = Timer.periodic(sweepOptions.interval, _sweep);
  }

  void _sweep(Timer timer) {
    sweep(sweepOptions.lifetime);
  }

  void sweep(Duration lifetime);
}
