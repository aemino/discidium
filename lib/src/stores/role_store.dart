part of 'store.dart';

class RoleStore extends ResourceStore<RoleSnowflake, Role> {
  RoleStore(Client client, StoreSpecifier storeSpecifier)
      : super(client, storeSpecifier);

  @override
  RoleSnowflake instantiateSnowflake(Json data) =>
      RoleSnowflake(client, data['id'].unwrap(), data['guild_id'].unwrap());

  @override
  Role instantiate(Json data) => Role(client, data);

  @override
  Future<Role> fetchFromApi(RoleSnowflake snowflake,
      {bool fromCache = true,
      bool fromStore = true,
      bool fromApi = true}) async {
    // TODO: Perhaps use restricted endpoint if the client user has manage roles
    // in this server.

    await snowflake.guild
        .fetch(fromCache: fromCache, fromStore: fromStore, fromApi: fromApi);

    return instantiate(get(snowflake.toString()));
  }
}
