part of 'store.dart';

class MemberStore extends UserStore {
  MemberStore(Client client, StoreSpecifier storeSpecifier)
      : super(client, storeSpecifier);

  @override
  MemberSnowflake instantiateSnowflake(Json data) =>
      MemberSnowflake(client, data['id'].unwrap(), data['guild_id'].unwrap());

  @override
  Member instantiate(Json data) => Member(client, data);

  @override
  Future<Member> fetchFromApi(covariant MemberSnowflake snowflake,
      {bool fromCache = true,
      bool fromStore = true,
      bool fromApi = true}) async {
    final data = await snowflake.route.get();
    if (data == null) return null;

    // await MemberCreateEvent.handle(client, data, artificial: true);
    return instantiate(data);
  }
}
