part of 'store.dart';

class GuildStore extends ResourceStore<GuildSnowflake, Guild> {
  GuildStore(Client client, StoreSpecifier storeSpecifier)
      : super(client, storeSpecifier);

  @override
  GuildSnowflake instantiateSnowflake(Json data) =>
      GuildSnowflake(client, data['id'].unwrap());

  @override
  Guild instantiate(Json data) => Guild(client, data);

  @override
  Future<Guild> fetchFromApi(GuildSnowflake snowflake,
      {bool fromCache = true,
      bool fromStore = true,
      bool fromApi = true}) async {
    final data = await snowflake.route.get();
    if (data == null) return null;

    await GuildCreateEvent.handle(client, null, data, artificial: true);
    return instantiate(data);
  }

  @override
  Json transform(Json data) {
    final roles = data['roles']?.asList();
    final channels = data['channels']?.asList();

    if (roles != null) {
      data['roles'] = roles.map((role) => role['id'].unwrap()).toList();
    }

    if (channels != null) {
      data['channels'] =
          channels.map((channel) => channel['id'].unwrap()).toList();
    }

    return data;
  }
}
