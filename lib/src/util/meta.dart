class Serializable {
  const Serializable();
}

class SerializableEnum {
  final List<Object> values;

  const SerializableEnum(this.values);
}

class Field {
  final String serializedName;

  final bool optional;
  final bool nullable;

  const Field(this.serializedName,
      {this.optional = false, this.nullable = false});
}
