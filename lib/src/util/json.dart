import 'dart:convert';

class Json<T> {
  final T _value;

  factory Json(T json) {
    if (json is Json) return json as Json; // ignore: unnecessary_cast
    return Json<T>._(json);
  }

  Json._(this._value);

  bool containsKey(Object key) {
    if (_value is Map)
      return (_value as Map).containsKey(key);
    else
      throw TypeError();
  }

  bool containsValue(Object value) {
    if (_value is Map)
      return (_value as Map).containsValue(value);
    else
      throw TypeError();
  }

  Json remove(Object key) {
    if (_value is List) {
      return Json((_value as List).removeAt(key));
    } else if (_value is Map) {
      return Json((_value as Map).remove(key));
    } else {
      throw TypeError();
    }
  }

  Json operator [](Object key) {
    if (_value is List) {
      return Json((_value as List)[key]);
    } else if (_value is Map) {
      if (!(_value as Map).containsKey(key)) return null;
      return Json((_value as Map)[key]);
    } else {
      throw TypeError();
    }
  }

  void operator []=(Object key, dynamic value) {
    if (_value is List) {
      (_value as List)[key] = value;
    } else if (_value is Map) {
      (_value as Map)[key] = value;
    } else {
      throw TypeError();
    }
  }

  Iterable<Json> asIterable() {
    if (_value is! List) throw TypeError();
    return (_value as List).map((v) => Json(v));
  }

  List<Json> asList() => asIterable().toList();

  Map<String, Json> asMap() {
    if (_value is! Map) throw TypeError();
    return (_value as Map).map((k, v) => MapEntry(k, Json(v)));
  }

  R unwrap<R>() => _value as R;

  Json<T> clone() => Json(json.decode(json.encode(_value)));
}
