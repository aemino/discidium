import 'dart:async';
import 'dart:io' as io;

import '../util/events.dart';

class WebSocket extends Stream {
  final EventStream<dynamic> onMessage = EventStream();
  final EventStream<dynamic> onError = EventStream();
  final EventStream<int> onClose = EventStream();

  io.WebSocket socket;
  StreamSubscription _subscription;

  WebSocket([this.socket]);

  static Future<WebSocket> fromRequest(io.HttpRequest req) async =>
      WebSocket(await io.WebSocketTransformer.upgrade(req));

  Future<void> connectSocket(Uri url) async {
    await close(1000);

    socket = await io.WebSocket.connect(url.toString());
    _subscription =
        listen(onSocketData, onError: onSocketError, onDone: onSocketDone);
  }

  @override
  StreamSubscription listen(void onData(event),
          {Function onError, void onDone(), bool cancelOnError = false}) =>
      socket.listen(onData,
          onError: onError, onDone: onDone, cancelOnError: cancelOnError);

  void onSocketData(event) => onMessage.add(event);
  void onSocketError(error) => onError.add(error);
  void onSocketDone() => onClose.add(socket.closeCode);
  void send(data) => socket?.add(data);

  Future<void> cleanup() async {
    await _subscription?.cancel();

    socket = null;
    _subscription = null;
  }

  Future<void> close([int code, String reason]) async {
    if (socket == null || socket.readyState == io.WebSocket.closed) {
      await cleanup();
      return;
    }

    await socket.close(code, reason);
    // TODO: Remove this redundancy check.
    // In Dart's WebSocket implementation, the WebSocket StreamController's
    // close method is being called without being awaited or submitted to
    // the future returned by WebSocket.close.
    // It seems that this is a bug. For now, we wait until the close event is
    // actually fired before continuing cleanup.
    await onClose.first;
    await cleanup();
  }

  Future<void> destroy() async {
    await close(1000);
    await destroyEvents();
  }

  void destroyEvents() =>
      Future.wait([onMessage.close(), onError.close(), onClose.close()]);
}
