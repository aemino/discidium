import 'dart:async';
import 'package:http/http.dart';

class ApiClient extends BaseClient {
  final Client _client = Client();

  final String authorization;
  final String userAgent;

  Map<String, String> get _headers => {
        'content-type': 'application/json',
        'authorization': authorization,
        'user-agent': userAgent
      };

  ApiClient(this.authorization, this.userAgent);

  @override
  Future<Response> send(Request request) => _client.send(request);

  Future<Response> request(String method, Uri url,
          {dynamic body, Map<String, String> headers = const {}}) =>
      send(
          Request(method, url, body: body, headers: _headers..addAll(headers)));

  Future<Response> multipart(String method, Uri url,
          {Map<String, String> fields,
          Iterable<MultipartFile> files,
          Map<String, String> headers = const {}}) =>
      send(Request.multipart(url,
          method: method,
          fields: fields,
          files: files,
          headers: _headers..addAll(headers)));
}
