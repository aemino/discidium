import 'dart:async';

import 'components/event/event_component.dart';
import 'components/event/events/event.dart';
import 'components/rest/rest_component.dart';
import 'components/rest/route_builder.dart';

import 'stores/store.dart';
import 'util/events.dart';

class ClientOptions {
  final bool autoShard;

  ClientOptions({this.autoShard = false});
}

/// A base class for interfacing with the Discord API.
class Client {
  RestComponent rest;
  EventComponent events;

  String token;
  ClientOptions options;

  /// Emits all client debug events, such as connection status.
  final EventStream<String> onDebug = EventStream();

  /// Emitted when one of this client's event pipelines becomes ready.
  final EventStream<ReadyEvent> onReady = EventStream();

  /// Emitted when a channel is created.
  final EventStream<ChannelCreateEvent> onChannelCreate = EventStream();

  /// Emitted when a channel is updated.
  final EventStream<ChannelUpdateEvent> onChannelUpdate = EventStream();

  /// Emitted when a channel is deleted.
  final EventStream<ChannelDeleteEvent> onChannelDelete = EventStream();

  /// Emitted when a user starts typing in a message channel.
  final EventStream<ChannelTypingStartEvent> onChannelTypingStart =
      EventStream();

  /// Emitted when a guild becomes available to this client.
  final EventStream<GuildCreateEvent> onGuildAvailable = EventStream();

  /// Emitted when this client is added to a guild.
  final EventStream<GuildCreateEvent> onGuildJoin = EventStream();

  /// Emitted when a guild is updated.
  final EventStream<GuildUpdateEvent> onGuildUpdate = EventStream();

  /// Emitted when a guild becomes unavailable to this client.
  final EventStream<GuildDeleteEvent> onGuildUnavailable = EventStream();

  /// Emitted when this client is removed from a guild.
  final EventStream<GuildDeleteEvent> onGuildLeave = EventStream();

  /// Emitted when a user is banned from a guild.
  final EventStream<GuildBanAddEvent> onGuildUserBan = EventStream();

  /// Emitted when a user is unbanned from a guild.
  final EventStream<GuildBanRemoveEvent> onGuildUserUnban = EventStream();

  /// Emitted when a message is sent.
  final EventStream<MessageCreateEvent> onMessageCreate = EventStream();

  /// Emitted when a message is updated.
  final EventStream<MessageUpdateEvent> onMessageUpdate = EventStream();

  /// Emitted when a message is deleted.
  final EventStream<MessageDeleteEvent> onMessageDelete = EventStream();

  /// Emitted when a user's presence is updated in a guild.
  final EventStream<PresenceUpdateEvent> onPresenceUpdate = EventStream();

  /// Emitted when a role is created.
  final EventStream<RoleCreateEvent> onRoleCreate = EventStream();

  /// Emitted when a role is updated.
  final EventStream<RoleUpdateEvent> onRoleUpdate = EventStream();

  /// Emitted when a role is deleted.
  final EventStream<RoleDeleteEvent> onRoleDelete = EventStream();

  /// Emitted when a user's properties are updated.
  final EventStream<UserUpdateEvent> onUserUpdate = EventStream();

  /// A store of all channels this client has access to.
  ChannelStore channels;

  /// A store of all guilds this client has access to.
  GuildStore guilds;

  /// A store of all guild members this client has access to.
  MemberStore members;

  /// A store of all messages this client has received.
  MessageStore messages;

  /// A store of all roles this client has access to.
  RoleStore roles;

  /// A store of all users this client has access to.
  UserStore users;

  Client(this.token, [this.options]) {
    rest = RestComponent(this);
    events = EventComponent(this);

    options ??= ClientOptions();

    BaseStore storeSpecifier() => MemoryStore(const SweepOptions());

    channels = ChannelStore(this, storeSpecifier);
    guilds = GuildStore(this, storeSpecifier);
    members = MemberStore(this, storeSpecifier);
    messages = MessageStore(this, storeSpecifier);
    roles = RoleStore(this, storeSpecifier);
    users = UserStore(this, storeSpecifier);
  }

  Route get api => rest.api;

  /// Attempts to initiate a WebSocket connection.
  Future<void> connect() async {
    final pipeline = events.createPipeline(const EventPipelineOptions(
        createGatewayConnection: true, handleDispatchEvents: true));
    final gateway = pipeline.gateway;

    await gateway.connect();
    gateway.onMessage.listen(pipeline.onIncomingPayload.add);
    pipeline.onOutgoingPayload.listen(gateway.send);
  }

  Future<void> destroy() async {
    await events.destroy();
    await _destroyEvents();
  }

  Future<void> _destroyEvents() => Future.wait([
        onDebug.close(),
        onReady.close(),
        onChannelCreate.close(),
        onChannelUpdate.close(),
        onChannelDelete.close(),
        onChannelTypingStart.close(),
        onGuildAvailable.close(),
        onGuildJoin.close(),
        onGuildUpdate.close(),
        onGuildUnavailable.close(),
        onGuildLeave.close(),
        onGuildUserBan.close(),
        onGuildUserUnban.close(),
        onMessageCreate.close(),
        onMessageUpdate.close(),
        onMessageDelete.close(),
        onPresenceUpdate.close(),
        onRoleCreate.close(),
        onRoleUpdate.close(),
        onRoleDelete.close(),
        onUserUpdate.close()
      ]);
}
