part of 'resource.dart';

abstract class _MemberEntity implements Entity {
  String get _guildID;

  /// The guild which this member is a part of.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  @override
  Route get route => client.api + 'guilds/$_guildID/members/$_id';

  @override
  MemberStore get store => client.members;
}

class MemberSnowflake<R extends Member> extends UserSnowflake<Member>
    with _MemberEntity {
  @override
  final String _guildID;

  MemberSnowflake(Client client, String id, this._guildID) : super(client, id);
}

abstract class Member extends Resource with _MemberEntity implements User {
  factory Member(Client client, Json data) = _$Member;

  Member._internal(Client client) : super._internal(client);

  @override
  MemberSnowflake get id => MemberSnowflake(client, _id, _guildID);

  @Field('guild_id')
  @override
  String get _guildID;

  @Field('user')
  Json get _user;

  /// The user object associated with this member.
  User get user => client.users.instantiate(_user);

  /// This user's nickname, if one has been set.
  @Field('nickname', optional: true)
  String get nickname;

  @Field('roles')
  List<String> get _roleIDs;

  /// The roles this user possesses.
  List<RoleSnowflake> get roles =>
      _roleIDs.map((roleID) => RoleSnowflake(client, roleID, _guildID));

  /// The time at which this member joined the associated guild.
  @Field('joined_at')
  DateTime get joinedAt;

  /// True if this user has been deafened.
  @Field('deaf')
  bool get isDeaf;

  /// True if this user has been muted.
  @Field('mute')
  bool get isMute;

  @override
  String get name => user.name;

  @override
  String get discriminator => user.discriminator;

  @override
  String get avatarHash => user.avatarHash;

  @override
  bool get isBot => user.isBot;

  @override
  bool get isMfaEnabled => user.isMfaEnabled;

  @override
  bool get isVerified => user.isVerified;

  @override
  String get email => user.email;

  /// The displayed name for this member.
  String get displayName => nickname ?? name;

  /// Compute the guild-level permissions for this member.
  Future<Permissions> permissions() async {
    final guild = await this.guild.fetch();
    if (guild.owner == id) return Permissions.all;

    final roles = await Future.wait(this.roles.map((role) => role.fetch()));
    var permissions = const Permissions();

    roles.forEach((role) => permissions |= role.permissions);
    return permissions;
  }

  Future<Permissions> permissionsIn(GuildChannelSnowflake channelID) async {
    var permissions = await this.permissions();

    final channel = await channelID.fetch();

    // TODO: This implementation is bad.
    final applicableOverwrites = channel.permissionOverwrites
        .where((overwrite) => overwrite.id == id || overwrite.id == guild);

    for (final overwrite in applicableOverwrites) {
      permissions |= overwrite.allow;
      permissions ^= overwrite.deny;
    }

    return permissions;
  }
}
