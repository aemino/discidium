part of 'resource.dart';

abstract class _MessageChannelEntity implements _ChannelEntity {
  /// Sends a message based on the options provided in the method arguments.
  ///
  /// This is a shorthand method for [sendMessage].
  Future<Message> send(String content) =>
      compose((builder) => builder..content = content).send();

  /// Sends a message using a message builder.
  Future<Message> sendMessage(MessageBuilder message) async {
    message._validate();

    final Json data =
        await (route + 'messages').post(message.toJson(), message._files);

    final event =
        await MessageCreateEvent.handle(client, null, data, artificial: true);
    return event.message;
  }

  BuiltMessage compose(MessageBuilder builder(MessageBuilder message)) =>
      builder(MessageBuilder())._build(this);
}

class MessageChannelSnowflake<R extends MessageChannel>
    extends ChannelSnowflake<R> with _MessageChannelEntity {
  MessageChannelSnowflake(Client client, String id) : super(client, id);
}

abstract class MessageChannel implements Channel, _MessageChannelEntity {
  /// The most recent message sent in this channel.
  @Field('last_message_id', nullable: true)
  String get _lastMessageID;

  MessageSnowflake get lastMessage =>
      MessageSnowflake(client, _lastMessageID, _id);

  /// The time at which the most recently pinned message was pinned.
  @Field('last_pin_timestamp', optional: true)
  DateTime get lastPinAt;
}

class BuiltMessage {
  final _MessageChannelEntity _channel;
  final MessageBuilder _message;

  BuiltMessage._internal(this._channel, this._message) {
    _message._validate();
  }

  Future<Message> send() => _channel.sendMessage(_message);
}

class MessageBuilder {
  String _content;

  /// The content of this message.
  String get content => _content;

  /// True if this should be a text-to-speech message.
  bool tts;

  /// The attachments for this message.
  List<Attachment> attachments = [];

  /// The embed to send with this message.
  EmbedBuilder embed;

  set content(String content) {
    RangeError.checkValueInInterval(content.length, 0, 2000);
    _content = content;
  }

  Iterable<http.MultipartFile> get _files {
    if (attachments.isEmpty) return null;

    var i = 0;
    return attachments.map((attach) => attach.asMultipartFile('file${i++}'));
  }

  void _validate() {
    if (embed != null) {
      RangeError.checkValueInInterval(embed.fields.length, 0, 25, 'fields');
    }
  }

  BuiltMessage _build(_MessageChannelEntity channel) =>
      BuiltMessage._internal(channel, this);

  Json toJson() {
    final json = <String, dynamic>{};

    if (content != null) json['content'] = content;
    if (tts != null) json['tts'] = tts;
    if (embed != null) json['embed'] = embed.toJson().unwrap();

    return Json(json);
  }
}

class Attachment {
  final List<int> _bytes;
  final String _filename;

  /// Creates an [Attachment] from a stream of bytes.
  static Future<Attachment> fromStream(Stream<List<int>> stream,
      [String filename]) async {
    final bytes = await collectBytes(stream);

    return Attachment.fromBytes(bytes, filename);
  }

  /// Creates an [Attachment] from a [File].
  ///
  /// If a filename is not provided, the name of the provided file will be used.
  static Future<Attachment> fromFile(File file, [String filename]) =>
      fromStream(file.openRead(), filename ?? basename(file.path));

  Attachment.fromBytes(this._bytes, [this._filename]);

  http.MultipartFile asMultipartFile(String name) =>
      http.MultipartFile(name, _bytes, filename: _filename);
}

class EmbedBuilder {
  String _title;

  int _color;

  String _description;

  /// The title of this embed.
  String get title => _title;

  /// The type of this embed.
  final EmbedType type = EmbedType.rich;

  /// The description of this embed.
  String get description => _description;

  /// The URL associated with this embed.
  String url;

  /// The relevant timestamp for this embed.
  DateTime timestamp;

  /// The color of this embed.
  int get color => _color;

  /// The footer of this embed.
  EmbedFooter footer;

  /// The image of this embed.
  EmbedImage image;

  /// The thumbnail of this embed.
  EmbedThumbnail thumbnail;

  /// The provider of this embed.
  EmbedProvider provider;

  /// The author of this embed.
  EmbedAuthor author;

  /// The fields in this embed.
  final List<EmbedField> fields = [];

  set title(String title) {
    RangeError.checkValueInInterval(title.length, 0, 256, 'title');
    _title = title;
  }

  set description(String description) {
    RangeError.checkValueInInterval(description.length, 0, 2048, 'description');
    _description = description;
  }

  set color(int color) {
    RangeError.checkValueInInterval(color, 0x000000, 0xffffff, 'color');
    _color = color;
  }

  Json toJson() => Json({
        'title': title,
        'description': description,
        'url': url,
        'timestamp': timestamp?.toIso8601String(),
        'color': color,
        'footer': footer?.toJson()?.unwrap(),
        'image': image?.toJson()?.unwrap(),
        'thumbnail': thumbnail?.toJson()?.unwrap(),
        'provider': provider?.toJson()?.unwrap(),
        'author': author?.toJson()?.unwrap()
      });
}

class EmbedFooter {
  /// The text of this embed footer.
  final String text;

  /// The source URL of this embed footer's icon. Can be an HTTP URL or a
  /// reference to an attachment.
  final Uri iconUrl;

  /// An optional proxy URL for the embed footer's icon.
  final Uri iconProxyUrl;

  EmbedFooter(this.text, {this.iconUrl, this.iconProxyUrl});

  Json toJson() => Json({
        'text': text,
        'icon_url': iconUrl?.toString(),
        'proxy_icon_url': iconProxyUrl?.toString()
      });
}

class EmbedImage {
  /// The source URL of this embed image. Can be an HTTP URL or a reference to
  /// an attachment.
  final Uri url;

  /// An optional proxy URL for the embed image.
  final Uri proxyUrl;

  /// The height of this embed image.
  final int height;

  /// The width of this embed image.
  final int width;

  EmbedImage(this.url, {this.proxyUrl, this.height, this.width});

  Json toJson() => Json({
        'url': url?.toString(),
        'proxy_url': proxyUrl?.toString(),
        'height': height,
        'width': width
      });
}

class EmbedThumbnail {
  /// The source URL of this embed thumbnail. Can be an HTTP URL or a reference
  /// to an attachment.
  final Uri url;

  /// An optional proxy URL for the embed thumbnail.
  final Uri proxyUrl;

  /// The height of this embed thumnail.
  final int height;

  /// The width of this embed thumnail.
  final int width;

  EmbedThumbnail(this.url, {this.proxyUrl, this.height, this.width});

  Json toJson() => Json({
        'url': url?.toString(),
        'proxy_url': proxyUrl?.toString(),
        'height': height,
        'width': width
      });
}

class EmbedProvider {
  /// The name of the embed provider.
  final String name;

  /// The URL of the embed provider.
  final Uri url;

  EmbedProvider(this.name, {this.url});

  Json toJson() => Json({'name': name, 'url': url?.toString()});
}

class EmbedAuthor {
  /// The name of the embed author.
  final String name;

  /// The URL of the embed author.
  final Uri url;

  /// The source URL of this embed author's icon. Can be an HTTP URL or a
  /// reference to an attachment.
  final Uri iconUrl;

  /// An optional proxy URL for the embed author's icon.
  final Uri iconProxyUrl;

  EmbedAuthor(this.name, {this.url, this.iconUrl, this.iconProxyUrl});

  Json toJson() => Json({
        'name': name,
        'url': url?.toString(),
        'icon_url': iconUrl?.toString(),
        'proxy_icon_url': iconProxyUrl?.toString()
      });
}

class EmbedField {
  /// The name of this embed field.
  final String name;

  /// The value of this embed field.
  final String value;

  /// True if this embed field should be displayed inline.
  final bool inline;

  EmbedField(this.name, this.value, {this.inline}) {
    RangeError.checkValueInInterval(name.length, 0, 256, 'name');
    RangeError.checkValueInInterval(value.length, 0, 1024, 'value');
  }

  Json toJson() => Json({'name': name, 'value': value, 'inline': inline});
}

enum EmbedType { rich }
