part of 'resource.dart';

abstract class _GuildChannelEntity implements _ChannelEntity {
  String get _guildID;

  /// The guild in which this channel resides.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);
}

class GuildChannelSnowflake<R extends GuildChannel> extends ChannelSnowflake<R>
    with _GuildChannelEntity {
  @override
  final String _guildID;

  GuildChannelSnowflake(Client client, String id, this._guildID)
      : super(client, id);
}

abstract class GuildChannel extends Channel with _GuildChannelEntity {
  factory GuildChannel(Client client, Json data) = _$GuildChannel;

  GuildChannel._internal(Client client) : super._internal(client);

  @override
  GuildChannelSnowflake get id => GuildChannelSnowflake(client, _id, _guildID);

  @Field('guild_id')
  @override
  String get _guildID;

  /// The position of this channel in the UI.
  @Field('position')
  int get position;

  @Field('permission_overwrites')
  Json get _permissionOverwrites;

  Iterable<PermissionOverwrite> get permissionOverwrites =>
      _permissionOverwrites
          .asIterable()
          .map((overwrite) => null);

  /// The name of this channel.
  @Field('name')
  String get name;

  @Field('parent_id', nullable: true)
  String get _parentID;

  /// The parent category channel of this channel, if there is one.
  CategoryChannelSnowflake get parent =>
      CategoryChannelSnowflake(client, _parentID, _guildID);
}
