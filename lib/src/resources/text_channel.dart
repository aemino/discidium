part of 'resource.dart';

abstract class _TextChannelEntity
    implements _GuildChannelEntity, _MessageChannelEntity {}

class TextChannelSnowflake<R extends TextChannel>
    extends GuildChannelSnowflake<R>
    with _TextChannelEntity, _MessageChannelEntity
    implements MessageChannelSnowflake<R> {
  TextChannelSnowflake(Client client, String id, String guildID)
      : super(client, id, guildID);
}

abstract class TextChannel extends GuildChannel
    with MessageChannel, _TextChannelEntity, _MessageChannelEntity {
  factory TextChannel(Client client, Json data) = _$TextChannel;

  TextChannel._internal(Client client) : super._internal(client);

  @override
  TextChannelSnowflake get id => TextChannelSnowflake(client, _id, _guildID);

  /// The topic blurb of this text channel.
  @Field('topic', nullable: true)
  String get topic;

  /// True if this channel has been marked as NSFW.
  @Field('nsfw', optional: true)
  bool get isNsfw;
}
