part of 'resource.dart';

abstract class _RoleEntity implements Entity {
  String get _guildID;

  /// The guild in which this role resides.
  GuildSnowflake get guild => GuildSnowflake(client, _guildID);

  @override
  Route get route => guild.route + 'roles/$_id';

  @override
  RoleStore get store => client.roles;
}

class RoleSnowflake<R extends Role> extends Snowflake<R> with _RoleEntity {
  @override
  final String _guildID;

  RoleSnowflake(Client client, String id, this._guildID) : super(client, id);
}

abstract class Role extends Resource with _RoleEntity {
  factory Role(Client client, Json data) = _$Role;

  Role._internal(Client client) : super._internal(client);

  @override
  RoleSnowflake get id => RoleSnowflake(client, _id, _guildID);

  /// The guild in which this role resides.
  @Field('guild_id')
  @override
  String get _guildID;

  /// The name of this role.
  @Field('name')
  String get name;

  /// The hexadecimal color of this role, represented as an integer.
  @Field('color')
  int get color;

  /// True if this role is separated from other roles in the UI.
  @Field('hoisted')
  bool get isHoisted;

  /// The position of this role in the UI.
  @Field('position')
  int get position;

  @Field('permissions')
  int get _permissions;

  /// The permissions assigned to this role.
  Permissions get permissions => Permissions(_permissions);

  /// True if this role is managed by a [Guild] integration.
  @Field('managed')
  bool get isManaged;

  /// True if this role can be mentioned in messages.
  @Field('mentionable')
  bool get isMentionable;

  /// True if this role's color counts toward the final computed color.
  bool get isColored => color != 0;
}
