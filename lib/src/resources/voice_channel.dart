part of 'resource.dart';

abstract class _VoiceChannelEntity implements _GuildChannelEntity {}

class VoiceChannelSnowflake<R extends VoiceChannel>
    extends GuildChannelSnowflake<R> with _VoiceChannelEntity {
  VoiceChannelSnowflake(Client client, String id, String guildID)
      : super(client, id, guildID);
}

abstract class VoiceChannel extends GuildChannel with _VoiceChannelEntity {
  factory VoiceChannel(Client client, Json data) = _$VoiceChannel;

  VoiceChannel._internal(Client client) : super._internal(client);

  @override
  VoiceChannelSnowflake get id => VoiceChannelSnowflake(client, _id, _guildID);

  /// The bitrate (in bits) of this voice channel.
  @Field('bitrate')
  int get bitrate;

  /// The user limit of this voice channel.
  @Field('user_limit')
  int get userLimit;
}
