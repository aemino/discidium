part of 'resource.dart';

abstract class _UserEntity implements Entity {
  @override
  Route get route => client.api + 'users/$_id';

  @override
  UserStore get store => client.users;
}

class UserSnowflake<R extends User> extends Snowflake<R> with _UserEntity {
  UserSnowflake(Client client, String id) : super(client, id);
}

abstract class User extends Resource with _UserEntity {
  factory User(Client client, Json data) = _$User;

  User._internal(Client client) : super._internal(client);

  @override
  UserSnowflake get id => UserSnowflake(client, _id);

  /// The name of this user.
  @Field('username')
  String get name;

  /// The discriminator of this user.
  @Field('discriminator')
  String get discriminator;

  /// The hash of this user's avatar.
  @Field('avatar', nullable: true)
  String get avatarHash;

  /// True if this user is a bot.
  @Field('bot', optional: true)
  bool get isBot;

  /// True if this user has MFA enabled.
  @Field('mfa_enabled', optional: true)
  bool get isMfaEnabled;

  /// True if this user is verified.
  /// 
  /// Only available with the `email` OAuth2 scope.
  @Field('verified', optional: true)
  bool get isVerified;

  /// The e-mail address of this user.
  ///
  /// Only available with the `email` OAuth2 scope.
  @Field('email', optional: true)
  String get email;
}
