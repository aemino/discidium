library discord.resources;

import 'dart:async';
import 'dart:io';

import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

import '../client.dart';
import '../components/rest/route_builder.dart';
import '../stores/store.dart';
import '../structures/structure.dart';
import '../util/json.dart';
import '../util/meta.dart';

part 'category_channel.dart';
part 'channel.dart';
part 'dm_channel.dart';
part 'guild.dart';
part 'guild_channel.dart';
part 'member.dart';
part 'message.dart';
part 'message_channel.dart';
part 'resource.g.dart';
part 'role.dart';
part 'text_channel.dart';
part 'user.dart';
part 'voice_channel.dart';

abstract class Entity {
  String get _id;
  Route get route;
  ResourceStore get store;

  final Client client;

  Entity(this.client);
}

abstract class Resource extends Entity {
  @Field('id')
  @override
  String get _id;

  /// The snowflake identifier for this resource.
  Snowflake get id;

  Resource._internal(Client client) : super(client);
}

abstract class Snowflake<R extends Resource> extends Entity {
  @override
  final String _id;

  Snowflake(Client client, this._id) : super(client);

  @override
  String toString() => _id;

  Future<R> fetch(
          {bool fromCache = true,
          bool fromStore = true,
          bool fromApi = true}) =>
      store
          .fetch(this,
              fromCache: fromCache, fromStore: fromStore, fromApi: fromApi)
          .then((result) => result as R);
}
