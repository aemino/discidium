// GENERATED CODE - DO NOT MODIFY BY HAND

part of discord.resources;

// **************************************************************************
// SerializableGenerator
// **************************************************************************

// ignore_for_file: annotate_overrides, unnecessary_const, unnecessary_new

abstract class _$BaseBuilder {
  Json toJson();
}

class _$CategoryChannelBuilder implements _$GuildChannelBuilder {
  String _id;

  ChannelType type;

  String _guildID;

  int position;

  Json _permissionOverwrites;

  String name;

  String _parentID;

  static _$CategoryChannelBuilder patch(
          _$CategoryChannelBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toChannelType[data['type'].unwrap()]
        .._guildID = data['guild_id'].unwrap()
        ..position = data['position'].unwrap()
        .._permissionOverwrites = data['permission_overwrites']
        ..name = data['name'].unwrap()
        .._parentID = data['parent_id'].unwrap();
  static _$CategoryChannelBuilder fromJson(Json data, Client client) =>
      _$CategoryChannelBuilder.patch(_$CategoryChannelBuilder(), data, client);
  static _$CategoryChannel build(
          _$CategoryChannelBuilder builder, Client client) =>
      new _$CategoryChannel._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromChannelType[type];
    if (_guildID != null) json['guild_id'] = _guildID;
    if (position != null) json['position'] = position;
    if (_permissionOverwrites != null)
      json['permission_overwrites'] = _permissionOverwrites;
    if (name != null) json['name'] = name;
    if (_parentID != null) json['parent_id'] = _parentID;
    return json;
  }
}

class _$CategoryChannel extends CategoryChannel with _CategoryChannelEntity {
  factory _$CategoryChannel(Client client, Json data) =>
      _$CategoryChannelBuilder.build(
          _$CategoryChannelBuilder.fromJson(data, client), client);

  _$CategoryChannel._fromBuilder(
      Client client, _$CategoryChannelBuilder builder)
      : _id = builder._id,
        type = builder.type,
        _guildID = builder._guildID,
        position = builder.position,
        _permissionOverwrites = builder._permissionOverwrites,
        name = builder.name,
        _parentID = builder._parentID,
        super._internal(client);

  final String _id;

  final ChannelType type;

  final String _guildID;

  final int position;

  final Json _permissionOverwrites;

  final String name;

  final String _parentID;
}

class _$ChannelBuilder implements _$BaseBuilder {
  String _id;

  ChannelType type;

  static _$ChannelBuilder patch(
          _$ChannelBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toChannelType[data['type'].unwrap()];
  static _$ChannelBuilder fromJson(Json data, Client client) =>
      _$ChannelBuilder.patch(_$ChannelBuilder(), data, client);
  static _$Channel build(_$ChannelBuilder builder, Client client) =>
      new _$Channel._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromChannelType[type];
    return json;
  }
}

class _$Channel extends Channel with _ChannelEntity {
  factory _$Channel(Client client, Json data) =>
      _$ChannelBuilder.build(_$ChannelBuilder.fromJson(data, client), client);

  _$Channel._fromBuilder(Client client, _$ChannelBuilder builder)
      : _id = builder._id,
        type = builder.type,
        super._internal(client);

  final String _id;

  final ChannelType type;
}

const _toChannelType = const {
  0: ChannelType.text,
  1: ChannelType.private,
  2: ChannelType.voice,
  3: ChannelType.group,
  4: ChannelType.category
};
const _fromChannelType = const {
  ChannelType.text: 0,
  ChannelType.private: 1,
  ChannelType.voice: 2,
  ChannelType.group: 3,
  ChannelType.category: 4
};

class _$DMChannelBuilder implements _$ChannelBuilder {
  String _id;

  ChannelType type;

  String _lastMessageID;

  DateTime lastPinAt;

  Json _recipients;

  static _$DMChannelBuilder patch(
          _$DMChannelBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toChannelType[data['type'].unwrap()]
        .._lastMessageID = data['last_message_id'].unwrap()
        ..lastPinAt = data.containsKey('last_pin_timestamp')
            ? DateTime.parse(data['last_pin_timestamp'].unwrap())
            : builder.lastPinAt
        .._recipients = data['recipients'];
  static _$DMChannelBuilder fromJson(Json data, Client client) =>
      _$DMChannelBuilder.patch(_$DMChannelBuilder(), data, client);
  static _$DMChannel build(_$DMChannelBuilder builder, Client client) =>
      new _$DMChannel._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromChannelType[type];
    if (_lastMessageID != null) json['last_message_id'] = _lastMessageID;
    if (lastPinAt != null)
      json['last_pin_timestamp'] = lastPinAt?.toIso8601String();
    if (_recipients != null) json['recipients'] = _recipients;
    return json;
  }
}

class _$DMChannel extends DMChannel
    with MessageChannel, _DMChannelEntity, _MessageChannelEntity {
  factory _$DMChannel(Client client, Json data) => _$DMChannelBuilder.build(
      _$DMChannelBuilder.fromJson(data, client), client);

  _$DMChannel._fromBuilder(Client client, _$DMChannelBuilder builder)
      : _id = builder._id,
        type = builder.type,
        _lastMessageID = builder._lastMessageID,
        lastPinAt = builder.lastPinAt,
        _recipients = builder._recipients,
        super._internal(client);

  final String _id;

  final ChannelType type;

  final String _lastMessageID;

  final DateTime lastPinAt;

  final Json _recipients;
}

class _$GroupDMChannelBuilder implements _$DMChannelBuilder {
  String _id;

  ChannelType type;

  String _lastMessageID;

  DateTime lastPinAt;

  Json _recipients;

  String _ownerID;

  static _$GroupDMChannelBuilder patch(
          _$GroupDMChannelBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toChannelType[data['type'].unwrap()]
        .._lastMessageID = data['last_message_id'].unwrap()
        ..lastPinAt = data.containsKey('last_pin_timestamp')
            ? DateTime.parse(data['last_pin_timestamp'].unwrap())
            : builder.lastPinAt
        .._recipients = data['recipients']
        .._ownerID = data['owner_id'].unwrap();
  static _$GroupDMChannelBuilder fromJson(Json data, Client client) =>
      _$GroupDMChannelBuilder.patch(_$GroupDMChannelBuilder(), data, client);
  static _$GroupDMChannel build(
          _$GroupDMChannelBuilder builder, Client client) =>
      new _$GroupDMChannel._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromChannelType[type];
    if (_lastMessageID != null) json['last_message_id'] = _lastMessageID;
    if (lastPinAt != null)
      json['last_pin_timestamp'] = lastPinAt?.toIso8601String();
    if (_recipients != null) json['recipients'] = _recipients;
    if (_ownerID != null) json['owner_id'] = _ownerID;
    return json;
  }
}

class _$GroupDMChannel extends GroupDMChannel
    with MessageChannel, _DMChannelEntity, _MessageChannelEntity {
  factory _$GroupDMChannel(Client client, Json data) => _$GroupDMChannelBuilder
      .build(_$GroupDMChannelBuilder.fromJson(data, client), client);

  _$GroupDMChannel._fromBuilder(Client client, _$GroupDMChannelBuilder builder)
      : _id = builder._id,
        type = builder.type,
        _lastMessageID = builder._lastMessageID,
        lastPinAt = builder.lastPinAt,
        _recipients = builder._recipients,
        _ownerID = builder._ownerID,
        super._internal(client);

  final String _id;

  final ChannelType type;

  final String _lastMessageID;

  final DateTime lastPinAt;

  final Json _recipients;

  final String _ownerID;
}

class _$GuildBuilder implements _$BaseBuilder {
  String _id;

  bool _unavailable;

  String name;

  String iconHash;

  String splashHash;

  bool isMine;

  String _ownerID;

  String region;

  String _afkChannelID;

  int _afkTimeout;

  bool isEmbedEnabled;

  String _embedChannelID;

  VerificationLevel verificationLevel;

  MessageNotificationLevel messageNotificationLevel;

  ExplicitContentFilterLevel explicitContentFilterLevel;

  List<String> features;

  MfaLevel mfaLevel;

  bool isWidgetEnabled;

  String _widgetChannelID;

  String _systemChannelID;

  DateTime joinedAt;

  bool isLarge;

  int memberCount;

  List<String> _roleIDs;

  List<String> _channelIDs;

  static _$GuildBuilder patch(
          _$GuildBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        .._unavailable = data['unavailable'].unwrap()
        ..name = data['name'].unwrap()
        ..iconHash = data['icon'].unwrap()
        ..splashHash = data['splash'].unwrap()
        ..isMine =
            data.containsKey('owner') ? data['owner'].unwrap() : builder.isMine
        .._ownerID = data['owner_id'].unwrap()
        ..region = data['region'].unwrap()
        .._afkChannelID = data['afk_channel_id'].unwrap()
        .._afkTimeout = data['afk_timeout'].unwrap()
        ..isEmbedEnabled = data.containsKey('embed_enabled')
            ? data['embed_enabled'].unwrap()
            : builder.isEmbedEnabled
        .._embedChannelID = data.containsKey('embed_channel_id')
            ? data['embed_channel_id'].unwrap()
            : builder._embedChannelID
        ..verificationLevel =
            _toVerificationLevel[data['verification_level'].unwrap()]
        ..messageNotificationLevel = _toMessageNotificationLevel[
            data['default_message_notifications'].unwrap()]
        ..explicitContentFilterLevel = _toExplicitContentFilterLevel[
            data['explicit_content_filter'].unwrap()]
        ..features = List.from(data['features'].unwrap())
        ..mfaLevel = _toMfaLevel[data['mfa_level'].unwrap()]
        ..isWidgetEnabled = data.containsKey('widget_enabled')
            ? data['widget_enabled'].unwrap()
            : builder.isWidgetEnabled
        .._widgetChannelID = data.containsKey('widget_channel_id')
            ? data['widget_channel_id'].unwrap()
            : builder._widgetChannelID
        .._systemChannelID = data['system_channel_id'].unwrap()
        ..joinedAt = data.containsKey('joined_at')
            ? DateTime.parse(data['joined_at'].unwrap())
            : builder.joinedAt
        ..isLarge =
            data.containsKey('large') ? data['large'].unwrap() : builder.isLarge
        ..memberCount = data.containsKey('member_count')
            ? data['member_count'].unwrap()
            : builder.memberCount
        .._roleIDs = List.from(data['roles'].unwrap())
        .._channelIDs = data.containsKey('channels')
            ? List.from(data['channels'].unwrap())
            : builder._channelIDs;
  static _$GuildBuilder fromJson(Json data, Client client) =>
      _$GuildBuilder.patch(_$GuildBuilder(), data, client);
  static _$Guild build(_$GuildBuilder builder, Client client) =>
      new _$Guild._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (_unavailable != null) json['unavailable'] = _unavailable;
    if (name != null) json['name'] = name;
    if (iconHash != null) json['icon'] = iconHash;
    if (splashHash != null) json['splash'] = splashHash;
    if (isMine != null) json['owner'] = isMine;
    if (_ownerID != null) json['owner_id'] = _ownerID;
    if (region != null) json['region'] = region;
    if (_afkChannelID != null) json['afk_channel_id'] = _afkChannelID;
    if (_afkTimeout != null) json['afk_timeout'] = _afkTimeout;
    if (isEmbedEnabled != null) json['embed_enabled'] = isEmbedEnabled;
    if (_embedChannelID != null) json['embed_channel_id'] = _embedChannelID;
    if (verificationLevel != null)
      json['verification_level'] = _fromVerificationLevel[verificationLevel];
    if (messageNotificationLevel != null)
      json['default_message_notifications'] =
          _fromMessageNotificationLevel[messageNotificationLevel];
    if (explicitContentFilterLevel != null)
      json['explicit_content_filter'] =
          _fromExplicitContentFilterLevel[explicitContentFilterLevel];
    if (features != null) json['features'] = features;
    if (mfaLevel != null) json['mfa_level'] = _fromMfaLevel[mfaLevel];
    if (isWidgetEnabled != null) json['widget_enabled'] = isWidgetEnabled;
    if (_widgetChannelID != null) json['widget_channel_id'] = _widgetChannelID;
    if (_systemChannelID != null) json['system_channel_id'] = _systemChannelID;
    if (joinedAt != null) json['joined_at'] = joinedAt?.toIso8601String();
    if (isLarge != null) json['large'] = isLarge;
    if (memberCount != null) json['member_count'] = memberCount;
    if (_roleIDs != null) json['roles'] = _roleIDs;
    if (_channelIDs != null) json['channels'] = _channelIDs;
    return json;
  }
}

class _$Guild extends Guild with _GuildEntity {
  factory _$Guild(Client client, Json data) =>
      _$GuildBuilder.build(_$GuildBuilder.fromJson(data, client), client);

  _$Guild._fromBuilder(Client client, _$GuildBuilder builder)
      : _id = builder._id,
        _unavailable = builder._unavailable,
        name = builder.name,
        iconHash = builder.iconHash,
        splashHash = builder.splashHash,
        isMine = builder.isMine,
        _ownerID = builder._ownerID,
        region = builder.region,
        _afkChannelID = builder._afkChannelID,
        _afkTimeout = builder._afkTimeout,
        isEmbedEnabled = builder.isEmbedEnabled,
        _embedChannelID = builder._embedChannelID,
        verificationLevel = builder.verificationLevel,
        messageNotificationLevel = builder.messageNotificationLevel,
        explicitContentFilterLevel = builder.explicitContentFilterLevel,
        features = builder.features,
        mfaLevel = builder.mfaLevel,
        isWidgetEnabled = builder.isWidgetEnabled,
        _widgetChannelID = builder._widgetChannelID,
        _systemChannelID = builder._systemChannelID,
        joinedAt = builder.joinedAt,
        isLarge = builder.isLarge,
        memberCount = builder.memberCount,
        _roleIDs = builder._roleIDs,
        _channelIDs = builder._channelIDs,
        super._internal(client);

  final String _id;

  final bool _unavailable;

  final String name;

  final String iconHash;

  final String splashHash;

  final bool isMine;

  final String _ownerID;

  final String region;

  final String _afkChannelID;

  final int _afkTimeout;

  final bool isEmbedEnabled;

  final String _embedChannelID;

  final VerificationLevel verificationLevel;

  final MessageNotificationLevel messageNotificationLevel;

  final ExplicitContentFilterLevel explicitContentFilterLevel;

  final List<String> features;

  final MfaLevel mfaLevel;

  final bool isWidgetEnabled;

  final String _widgetChannelID;

  final String _systemChannelID;

  final DateTime joinedAt;

  final bool isLarge;

  final int memberCount;

  final List<String> _roleIDs;

  final List<String> _channelIDs;
}

const _toVerificationLevel = const {
  0: VerificationLevel.none,
  1: VerificationLevel.low,
  2: VerificationLevel.medium,
  3: VerificationLevel.high,
  4: VerificationLevel.veryHigh
};
const _fromVerificationLevel = const {
  VerificationLevel.none: 0,
  VerificationLevel.low: 1,
  VerificationLevel.medium: 2,
  VerificationLevel.high: 3,
  VerificationLevel.veryHigh: 4
};

const _toMessageNotificationLevel = const {
  0: MessageNotificationLevel.all,
  1: MessageNotificationLevel.mentions
};
const _fromMessageNotificationLevel = const {
  MessageNotificationLevel.all: 0,
  MessageNotificationLevel.mentions: 1
};

const _toExplicitContentFilterLevel = const {
  0: ExplicitContentFilterLevel.disabled,
  1: ExplicitContentFilterLevel.membersWithoutRoles,
  2: ExplicitContentFilterLevel.allMembers
};
const _fromExplicitContentFilterLevel = const {
  ExplicitContentFilterLevel.disabled: 0,
  ExplicitContentFilterLevel.membersWithoutRoles: 1,
  ExplicitContentFilterLevel.allMembers: 2
};

const _toMfaLevel = const {0: MfaLevel.none, 1: MfaLevel.elevated};
const _fromMfaLevel = const {MfaLevel.none: 0, MfaLevel.elevated: 1};

class _$GuildChannelBuilder implements _$ChannelBuilder {
  String _id;

  ChannelType type;

  String _guildID;

  int position;

  Json _permissionOverwrites;

  String name;

  String _parentID;

  static _$GuildChannelBuilder patch(
          _$GuildChannelBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toChannelType[data['type'].unwrap()]
        .._guildID = data['guild_id'].unwrap()
        ..position = data['position'].unwrap()
        .._permissionOverwrites = data['permission_overwrites']
        ..name = data['name'].unwrap()
        .._parentID = data['parent_id'].unwrap();
  static _$GuildChannelBuilder fromJson(Json data, Client client) =>
      _$GuildChannelBuilder.patch(_$GuildChannelBuilder(), data, client);
  static _$GuildChannel build(_$GuildChannelBuilder builder, Client client) =>
      new _$GuildChannel._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromChannelType[type];
    if (_guildID != null) json['guild_id'] = _guildID;
    if (position != null) json['position'] = position;
    if (_permissionOverwrites != null)
      json['permission_overwrites'] = _permissionOverwrites;
    if (name != null) json['name'] = name;
    if (_parentID != null) json['parent_id'] = _parentID;
    return json;
  }
}

class _$GuildChannel extends GuildChannel with _GuildChannelEntity {
  factory _$GuildChannel(Client client, Json data) => _$GuildChannelBuilder
      .build(_$GuildChannelBuilder.fromJson(data, client), client);

  _$GuildChannel._fromBuilder(Client client, _$GuildChannelBuilder builder)
      : _id = builder._id,
        type = builder.type,
        _guildID = builder._guildID,
        position = builder.position,
        _permissionOverwrites = builder._permissionOverwrites,
        name = builder.name,
        _parentID = builder._parentID,
        super._internal(client);

  final String _id;

  final ChannelType type;

  final String _guildID;

  final int position;

  final Json _permissionOverwrites;

  final String name;

  final String _parentID;
}

class _$MemberBuilder implements _$BaseBuilder {
  String _id;

  String _guildID;

  Json _user;

  String nickname;

  List<String> _roleIDs;

  DateTime joinedAt;

  bool isDeaf;

  bool isMute;

  static _$MemberBuilder patch(
          _$MemberBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        .._guildID = data['guild_id'].unwrap()
        .._user = data['user']
        ..nickname = data.containsKey('nickname')
            ? data['nickname'].unwrap()
            : builder.nickname
        .._roleIDs = List.from(data['roles'].unwrap())
        ..joinedAt = DateTime.parse(data['joined_at'].unwrap())
        ..isDeaf = data['deaf'].unwrap()
        ..isMute = data['mute'].unwrap();
  static _$MemberBuilder fromJson(Json data, Client client) =>
      _$MemberBuilder.patch(_$MemberBuilder(), data, client);
  static _$Member build(_$MemberBuilder builder, Client client) =>
      new _$Member._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (_guildID != null) json['guild_id'] = _guildID;
    if (_user != null) json['user'] = _user;
    if (nickname != null) json['nickname'] = nickname;
    if (_roleIDs != null) json['roles'] = _roleIDs;
    if (joinedAt != null) json['joined_at'] = joinedAt?.toIso8601String();
    if (isDeaf != null) json['deaf'] = isDeaf;
    if (isMute != null) json['mute'] = isMute;
    return json;
  }
}

class _$Member extends Member with _MemberEntity implements User {
  factory _$Member(Client client, Json data) =>
      _$MemberBuilder.build(_$MemberBuilder.fromJson(data, client), client);

  _$Member._fromBuilder(Client client, _$MemberBuilder builder)
      : _id = builder._id,
        _guildID = builder._guildID,
        _user = builder._user,
        nickname = builder.nickname,
        _roleIDs = builder._roleIDs,
        joinedAt = builder.joinedAt,
        isDeaf = builder.isDeaf,
        isMute = builder.isMute,
        super._internal(client);

  final String _id;

  final String _guildID;

  final Json _user;

  final String nickname;

  final List<String> _roleIDs;

  final DateTime joinedAt;

  final bool isDeaf;

  final bool isMute;
}

class _$MessageBuilder implements _$BaseBuilder {
  String _id;

  String _channelID;

  Json _author;

  String content;

  DateTime sentAt;

  DateTime lastEditedAt;

  bool isTts;

  bool isMentioningEveryone;

  List<String> _mentionedUserIDs;

  List<String> _mentionedRoleIDs;

  bool isPinned;

  MessageType type;

  static _$MessageBuilder patch(
          _$MessageBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        .._channelID = data['channel_id'].unwrap()
        .._author = data['author']
        ..content = data['content'].unwrap()
        ..sentAt = DateTime.parse(data['timestamp'].unwrap())
        ..lastEditedAt = (data['edited_timestamp'].unwrap() == null
            ? null
            : DateTime.parse(data['edited_timestamp'].unwrap()))
        ..isTts = data['tts'].unwrap()
        ..isMentioningEveryone = data['mention_everyone'].unwrap()
        .._mentionedUserIDs = List.from(data['mentions'].unwrap())
        .._mentionedRoleIDs = List.from(data['mention_roles'].unwrap())
        ..isPinned = data['pinned'].unwrap()
        ..type = _toMessageType[data['type'].unwrap()];
  static _$MessageBuilder fromJson(Json data, Client client) =>
      _$MessageBuilder.patch(_$MessageBuilder(), data, client);
  static _$Message build(_$MessageBuilder builder, Client client) =>
      new _$Message._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (_channelID != null) json['channel_id'] = _channelID;
    if (_author != null) json['author'] = _author;
    if (content != null) json['content'] = content;
    if (sentAt != null) json['timestamp'] = sentAt?.toIso8601String();
    if (lastEditedAt != null)
      json['edited_timestamp'] = lastEditedAt?.toIso8601String();
    if (isTts != null) json['tts'] = isTts;
    if (isMentioningEveryone != null)
      json['mention_everyone'] = isMentioningEveryone;
    if (_mentionedUserIDs != null) json['mentions'] = _mentionedUserIDs;
    if (_mentionedRoleIDs != null) json['mention_roles'] = _mentionedRoleIDs;
    if (isPinned != null) json['pinned'] = isPinned;
    if (type != null) json['type'] = _fromMessageType[type];
    return json;
  }
}

class _$Message extends Message with _MessageEntity {
  factory _$Message(Client client, Json data) =>
      _$MessageBuilder.build(_$MessageBuilder.fromJson(data, client), client);

  _$Message._fromBuilder(Client client, _$MessageBuilder builder)
      : _id = builder._id,
        _channelID = builder._channelID,
        _author = builder._author,
        content = builder.content,
        sentAt = builder.sentAt,
        lastEditedAt = builder.lastEditedAt,
        isTts = builder.isTts,
        isMentioningEveryone = builder.isMentioningEveryone,
        _mentionedUserIDs = builder._mentionedUserIDs,
        _mentionedRoleIDs = builder._mentionedRoleIDs,
        isPinned = builder.isPinned,
        type = builder.type,
        super._internal(client);

  final String _id;

  final String _channelID;

  final Json _author;

  final String content;

  final DateTime sentAt;

  final DateTime lastEditedAt;

  final bool isTts;

  final bool isMentioningEveryone;

  final List<String> _mentionedUserIDs;

  final List<String> _mentionedRoleIDs;

  final bool isPinned;

  final MessageType type;
}

const _toMessageType = const {
  0: MessageType.normal,
  1: MessageType.recipientAdd,
  2: MessageType.recipientRemove,
  3: MessageType.call,
  4: MessageType.channelNameUpdate,
  5: MessageType.channelIconUpdate,
  6: MessageType.channelMessagePin,
  7: MessageType.guildMemberJoin
};
const _fromMessageType = const {
  MessageType.normal: 0,
  MessageType.recipientAdd: 1,
  MessageType.recipientRemove: 2,
  MessageType.call: 3,
  MessageType.channelNameUpdate: 4,
  MessageType.channelIconUpdate: 5,
  MessageType.channelMessagePin: 6,
  MessageType.guildMemberJoin: 7
};

class _$RoleBuilder implements _$BaseBuilder {
  String _id;

  String _guildID;

  String name;

  int color;

  bool isHoisted;

  int position;

  int _permissions;

  bool isManaged;

  bool isMentionable;

  static _$RoleBuilder patch(_$RoleBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        .._guildID = data['guild_id'].unwrap()
        ..name = data['name'].unwrap()
        ..color = data['color'].unwrap()
        ..isHoisted = data['hoisted'].unwrap()
        ..position = data['position'].unwrap()
        .._permissions = data['permissions'].unwrap()
        ..isManaged = data['managed'].unwrap()
        ..isMentionable = data['mentionable'].unwrap();
  static _$RoleBuilder fromJson(Json data, Client client) =>
      _$RoleBuilder.patch(_$RoleBuilder(), data, client);
  static _$Role build(_$RoleBuilder builder, Client client) =>
      new _$Role._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (_guildID != null) json['guild_id'] = _guildID;
    if (name != null) json['name'] = name;
    if (color != null) json['color'] = color;
    if (isHoisted != null) json['hoisted'] = isHoisted;
    if (position != null) json['position'] = position;
    if (_permissions != null) json['permissions'] = _permissions;
    if (isManaged != null) json['managed'] = isManaged;
    if (isMentionable != null) json['mentionable'] = isMentionable;
    return json;
  }
}

class _$Role extends Role with _RoleEntity {
  factory _$Role(Client client, Json data) =>
      _$RoleBuilder.build(_$RoleBuilder.fromJson(data, client), client);

  _$Role._fromBuilder(Client client, _$RoleBuilder builder)
      : _id = builder._id,
        _guildID = builder._guildID,
        name = builder.name,
        color = builder.color,
        isHoisted = builder.isHoisted,
        position = builder.position,
        _permissions = builder._permissions,
        isManaged = builder.isManaged,
        isMentionable = builder.isMentionable,
        super._internal(client);

  final String _id;

  final String _guildID;

  final String name;

  final int color;

  final bool isHoisted;

  final int position;

  final int _permissions;

  final bool isManaged;

  final bool isMentionable;
}

class _$TextChannelBuilder implements _$GuildChannelBuilder {
  String _id;

  ChannelType type;

  String _guildID;

  int position;

  Json _permissionOverwrites;

  String name;

  String _parentID;

  String _lastMessageID;

  DateTime lastPinAt;

  String topic;

  bool isNsfw;

  static _$TextChannelBuilder patch(
          _$TextChannelBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toChannelType[data['type'].unwrap()]
        .._guildID = data['guild_id'].unwrap()
        ..position = data['position'].unwrap()
        .._permissionOverwrites = data['permission_overwrites']
        ..name = data['name'].unwrap()
        .._parentID = data['parent_id'].unwrap()
        .._lastMessageID = data['last_message_id'].unwrap()
        ..lastPinAt = data.containsKey('last_pin_timestamp')
            ? DateTime.parse(data['last_pin_timestamp'].unwrap())
            : builder.lastPinAt
        ..topic = data['topic'].unwrap()
        ..isNsfw =
            data.containsKey('nsfw') ? data['nsfw'].unwrap() : builder.isNsfw;
  static _$TextChannelBuilder fromJson(Json data, Client client) =>
      _$TextChannelBuilder.patch(_$TextChannelBuilder(), data, client);
  static _$TextChannel build(_$TextChannelBuilder builder, Client client) =>
      new _$TextChannel._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromChannelType[type];
    if (_guildID != null) json['guild_id'] = _guildID;
    if (position != null) json['position'] = position;
    if (_permissionOverwrites != null)
      json['permission_overwrites'] = _permissionOverwrites;
    if (name != null) json['name'] = name;
    if (_parentID != null) json['parent_id'] = _parentID;
    if (_lastMessageID != null) json['last_message_id'] = _lastMessageID;
    if (lastPinAt != null)
      json['last_pin_timestamp'] = lastPinAt?.toIso8601String();
    if (topic != null) json['topic'] = topic;
    if (isNsfw != null) json['nsfw'] = isNsfw;
    return json;
  }
}

class _$TextChannel extends TextChannel
    with MessageChannel, _TextChannelEntity, _MessageChannelEntity {
  factory _$TextChannel(Client client, Json data) => _$TextChannelBuilder.build(
      _$TextChannelBuilder.fromJson(data, client), client);

  _$TextChannel._fromBuilder(Client client, _$TextChannelBuilder builder)
      : _id = builder._id,
        type = builder.type,
        _guildID = builder._guildID,
        position = builder.position,
        _permissionOverwrites = builder._permissionOverwrites,
        name = builder.name,
        _parentID = builder._parentID,
        _lastMessageID = builder._lastMessageID,
        lastPinAt = builder.lastPinAt,
        topic = builder.topic,
        isNsfw = builder.isNsfw,
        super._internal(client);

  final String _id;

  final ChannelType type;

  final String _guildID;

  final int position;

  final Json _permissionOverwrites;

  final String name;

  final String _parentID;

  final String _lastMessageID;

  final DateTime lastPinAt;

  final String topic;

  final bool isNsfw;
}

class _$UserBuilder implements _$BaseBuilder {
  String _id;

  String name;

  String discriminator;

  String avatarHash;

  bool isBot;

  bool isMfaEnabled;

  bool isVerified;

  String email;

  static _$UserBuilder patch(_$UserBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..name = data['username'].unwrap()
        ..discriminator = data['discriminator'].unwrap()
        ..avatarHash = data['avatar'].unwrap()
        ..isBot = data.containsKey('bot') ? data['bot'].unwrap() : builder.isBot
        ..isMfaEnabled = data.containsKey('mfa_enabled')
            ? data['mfa_enabled'].unwrap()
            : builder.isMfaEnabled
        ..isVerified = data.containsKey('verified')
            ? data['verified'].unwrap()
            : builder.isVerified
        ..email =
            data.containsKey('email') ? data['email'].unwrap() : builder.email;
  static _$UserBuilder fromJson(Json data, Client client) =>
      _$UserBuilder.patch(_$UserBuilder(), data, client);
  static _$User build(_$UserBuilder builder, Client client) =>
      new _$User._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (name != null) json['username'] = name;
    if (discriminator != null) json['discriminator'] = discriminator;
    if (avatarHash != null) json['avatar'] = avatarHash;
    if (isBot != null) json['bot'] = isBot;
    if (isMfaEnabled != null) json['mfa_enabled'] = isMfaEnabled;
    if (isVerified != null) json['verified'] = isVerified;
    if (email != null) json['email'] = email;
    return json;
  }
}

class _$User extends User with _UserEntity {
  factory _$User(Client client, Json data) =>
      _$UserBuilder.build(_$UserBuilder.fromJson(data, client), client);

  _$User._fromBuilder(Client client, _$UserBuilder builder)
      : _id = builder._id,
        name = builder.name,
        discriminator = builder.discriminator,
        avatarHash = builder.avatarHash,
        isBot = builder.isBot,
        isMfaEnabled = builder.isMfaEnabled,
        isVerified = builder.isVerified,
        email = builder.email,
        super._internal(client);

  final String _id;

  final String name;

  final String discriminator;

  final String avatarHash;

  final bool isBot;

  final bool isMfaEnabled;

  final bool isVerified;

  final String email;
}

class _$VoiceChannelBuilder implements _$GuildChannelBuilder {
  String _id;

  ChannelType type;

  String _guildID;

  int position;

  Json _permissionOverwrites;

  String name;

  String _parentID;

  int bitrate;

  int userLimit;

  static _$VoiceChannelBuilder patch(
          _$VoiceChannelBuilder builder, Json data, Client client) =>
      builder
        .._id = data['id'].unwrap()
        ..type = _toChannelType[data['type'].unwrap()]
        .._guildID = data['guild_id'].unwrap()
        ..position = data['position'].unwrap()
        .._permissionOverwrites = data['permission_overwrites']
        ..name = data['name'].unwrap()
        .._parentID = data['parent_id'].unwrap()
        ..bitrate = data['bitrate'].unwrap()
        ..userLimit = data['user_limit'].unwrap();
  static _$VoiceChannelBuilder fromJson(Json data, Client client) =>
      _$VoiceChannelBuilder.patch(_$VoiceChannelBuilder(), data, client);
  static _$VoiceChannel build(_$VoiceChannelBuilder builder, Client client) =>
      new _$VoiceChannel._fromBuilder(client, builder);
  Json toJson() {
    final json = new Json({});
    if (_id != null) json['id'] = _id;
    if (type != null) json['type'] = _fromChannelType[type];
    if (_guildID != null) json['guild_id'] = _guildID;
    if (position != null) json['position'] = position;
    if (_permissionOverwrites != null)
      json['permission_overwrites'] = _permissionOverwrites;
    if (name != null) json['name'] = name;
    if (_parentID != null) json['parent_id'] = _parentID;
    if (bitrate != null) json['bitrate'] = bitrate;
    if (userLimit != null) json['user_limit'] = userLimit;
    return json;
  }
}

class _$VoiceChannel extends VoiceChannel with _VoiceChannelEntity {
  factory _$VoiceChannel(Client client, Json data) => _$VoiceChannelBuilder
      .build(_$VoiceChannelBuilder.fromJson(data, client), client);

  _$VoiceChannel._fromBuilder(Client client, _$VoiceChannelBuilder builder)
      : _id = builder._id,
        type = builder.type,
        _guildID = builder._guildID,
        position = builder.position,
        _permissionOverwrites = builder._permissionOverwrites,
        name = builder.name,
        _parentID = builder._parentID,
        bitrate = builder.bitrate,
        userLimit = builder.userLimit,
        super._internal(client);

  final String _id;

  final ChannelType type;

  final String _guildID;

  final int position;

  final Json _permissionOverwrites;

  final String name;

  final String _parentID;

  final int bitrate;

  final int userLimit;
}
