part of 'resource.dart';

abstract class _MessageEntity implements Entity {
  String get _channelID;

  /// The channel in which this message resides.
  MessageChannelSnowflake get channel =>
      MessageChannelSnowflake(client, _channelID);

  @override
  Route get route => client.api + 'channels/$_channelID/messages/$_id';
  
  @override
  MessageStore get store => client.messages;
}

class MessageSnowflake<R extends Message> extends Snowflake<R>
    with _MessageEntity {
  @override
  final String _channelID;

  MessageSnowflake(Client client, String id, this._channelID)
      : super(client, id);
}

abstract class Message extends Resource with _MessageEntity {
  factory Message(Client client, Json data) = _$Message;

  Message._internal(Client client) : super._internal(client);

  @override
  MessageSnowflake get id => MessageSnowflake(client, _id, _channelID);

  @Field('channel_id')
  @override
  String get _channelID;

  @Field('author')
  Json get _author;

  /// The user or webhook that sent this message.
  User get author => client.users.instantiate(_author);

  /// The content of this message.
  @Field('content')
  String get content;

  /// The time at which this message was sent.
  @Field('timestamp')
  DateTime get sentAt;

  /// The time at which this message was last edited.
  @Field('edited_timestamp', nullable: true)
  DateTime get lastEditedAt;

  /// True if this message is a text-to-speech message.
  @Field('tts')
  bool get isTts;

  /// True if this message mentions everyone.
  @Field('mention_everyone')
  bool get isMentioningEveryone;

  @Field('mentions')
  List<String> get _mentionedUserIDs;

  /// The users mentioned by this message;
  Iterable<UserSnowflake> get mentionedUsers =>
      _mentionedUserIDs.map((userID) => UserSnowflake(client, userID));

  @Field('mention_roles')
  List<String> get _mentionedRoleIDs;

  /// The roles mentioned by this message;
  List<RoleSnowflake> get mentionedRoles => _mentionedRoleIDs.map((roleID) =>
      RoleSnowflake(client, roleID, (channel as GuildChannel)._guildID));

  /// True if this message has been pinned.
  @Field('pinned')
  bool get isPinned;

  /// The type of this message.
  @Field('type')
  MessageType get type;
}

@SerializableEnum([0, 1, 2, 3, 4, 5, 6, 7])
enum MessageType {
  normal,
  recipientAdd,
  recipientRemove,
  call,
  channelNameUpdate,
  channelIconUpdate,
  channelMessagePin,
  guildMemberJoin
}
