part of 'resource.dart';

abstract class _CategoryChannelEntity implements _GuildChannelEntity {}

class CategoryChannelSnowflake<R extends CategoryChannel>
    extends GuildChannelSnowflake<R> with _CategoryChannelEntity {
  CategoryChannelSnowflake(Client client, String id, String guildID)
      : super(client, id, guildID);
}

abstract class CategoryChannel extends GuildChannel
    with _CategoryChannelEntity {
  factory CategoryChannel(Client client, Json data) = _$CategoryChannel;

  CategoryChannel._internal(Client client) : super._internal(client);

  @override
  CategoryChannelSnowflake get id =>
      CategoryChannelSnowflake(client, _id, _guildID);
}
