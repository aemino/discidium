part of 'resource.dart';

abstract class _ChannelEntity implements Entity {
  @override
  Route get route => client.api + 'channels/$_id';

  @override
  ChannelStore get store => client.channels;
}

class ChannelSnowflake<R extends Channel> extends Snowflake<R>
    with _ChannelEntity {
  ChannelSnowflake(Client client, String id) : super(client, id);
}

abstract class Channel extends Resource with _ChannelEntity {
  factory Channel(Client client, Json data) {
    final ChannelType type = _toChannelType[data['type'].unwrap()];

    switch (type) {
      case ChannelType.text:
        return TextChannel(client, data);
      case ChannelType.private:
        return DMChannel(client, data);
      case ChannelType.voice:
        return VoiceChannel(client, data);
      case ChannelType.group:
        return GroupDMChannel(client, data);
      case ChannelType.category:
        return CategoryChannel(client, data);
      default:
        throw CastError();
    }
  }

  Channel._internal(Client client) : super._internal(client);

  @override
  ChannelSnowflake get id => ChannelSnowflake(client, _id);

  /// The type of this channel.
  @Field('type')
  ChannelType get type;
}

@SerializableEnum([0, 1, 2, 3, 4])
enum ChannelType { text, private, voice, group, category }
