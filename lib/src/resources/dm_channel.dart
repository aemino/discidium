part of 'resource.dart';

abstract class _DMChannelEntity implements _ChannelEntity, _MessageChannelEntity {}

class DMChannelSnowflake<R extends DMChannel> extends ChannelSnowflake<R>
    with _DMChannelEntity, _MessageChannelEntity
    implements MessageChannelSnowflake<R> {
  DMChannelSnowflake(Client client, String id) : super(client, id);
}

abstract class DMChannel extends Channel
    with MessageChannel, _DMChannelEntity, _MessageChannelEntity {
  factory DMChannel(Client client, Json data) = _$DMChannel;

  DMChannel._internal(Client client) : super._internal(client);

  @override
  DMChannelSnowflake get id => DMChannelSnowflake(client, _id);

  @Field('recipients')
  Json get _recipients;

  /// The recipients in this direct message channel.
  Iterable<User> get recipients =>
      _recipients.asIterable().map((user) => client.users.instantiate(user));
}

class GroupDMChannelSnowflake<R extends GroupDMChannel>
    extends DMChannelSnowflake<R> with _DMChannelEntity, _MessageChannelEntity {
  GroupDMChannelSnowflake(Client client, String id) : super(client, id);
}

abstract class GroupDMChannel extends DMChannel
    with MessageChannel, _DMChannelEntity, _MessageChannelEntity {
  factory GroupDMChannel(Client client, Json data) = _$GroupDMChannel;

  GroupDMChannel._internal(Client client) : super._internal(client);

  @override
  GroupDMChannelSnowflake get id => GroupDMChannelSnowflake(client, _id);

  @Field('owner_id')
  String get _ownerID;

  /// The owner of this direct message group channel.
  UserSnowflake get owner => UserSnowflake(client, _ownerID);
}
