part of 'resource.dart';

abstract class _GuildEntity implements Entity {
  @override
  Route get route => client.api + 'guilds/$_id';

  @override
  GuildStore get store => client.guilds;

  MemberSnowflake member(UserSnowflake user) =>
      MemberSnowflake(client, user._id, _id);
}

class GuildSnowflake<R extends Guild> extends Snowflake<R> with _GuildEntity {
  GuildSnowflake(Client client, String id) : super(client, id);
}

abstract class Guild extends Resource with _GuildEntity {
  factory Guild(Client client, Json data) = _$Guild;

  Guild._internal(Client client) : super._internal(client);

  @override
  GuildSnowflake get id => GuildSnowflake(client, _id);

  @Field('unavailable')
  bool get _unavailable;

  /// True if this guild is available.
  bool get available => !_unavailable;

  /// The name of this guild.
  @Field('name')
  String get name;

  /// The hash of this guild's icon.
  @Field('icon', nullable: true)
  String get iconHash;

  /// The hash of this guild's splash.
  @Field('splash', nullable: true)
  String get splashHash;

  /// True if the current user owns this guild.
  @Field('owner', optional: true)
  bool get isMine;

  @Field('owner_id', nullable: true)
  String get _ownerID;

  /// The owner of this guild.
  MemberSnowflake get owner => MemberSnowflake(client, _ownerID, _id);

  // TODO: permissions

  /// The id of this guild's region.
  @Field('region')
  String get region;

  @Field('afk_channel_id')
  String get _afkChannelID;

  /// This guild's designated AFK voice channel, if one has been set.
  VoiceChannelSnowflake get afkChannel => _afkChannelID == null
      ? null
      : VoiceChannelSnowflake(client, _afkChannelID, _id);

  @Field('afk_timeout')
  int get _afkTimeout;

  /// This guild's designated AFK timeout.
  Duration get afkTimeout => Duration(seconds: _afkTimeout);

  /// True if this guild has embedding enabled.
  @Field('embed_enabled', optional: true)
  bool get isEmbedEnabled;

  @Field('embed_channel_id', optional: true)
  String get _embedChannelID;

  /// This guild's designated embedded channel, if one has been set.
  GuildChannelSnowflake get embedChannel => _embedChannelID == null
      ? null
      : GuildChannelSnowflake(client, _embedChannelID, _id);

  /// The verification level of this guild.
  @Field('verification_level')
  VerificationLevel get verificationLevel;

  /// The default message notification level of this guild.
  @Field('default_message_notifications')
  MessageNotificationLevel get messageNotificationLevel;

  /// The explicit content filter level of this guild.
  @Field('explicit_content_filter')
  ExplicitContentFilterLevel get explicitContentFilterLevel;

  /// The enabled features for this guild.
  @Field('features')
  List<String> get features;

  /// The multi-factor authentication level of this guild.
  @Field('mfa_level')
  MfaLevel get mfaLevel;

  // TODO: application_id

  /// True if this guild has a widget enabled.
  @Field('widget_enabled', optional: true)
  bool get isWidgetEnabled;

  @Field('widget_channel_id', optional: true)
  String get _widgetChannelID;

  /// This guild's designated widget channel, if one has been set.
  GuildChannelSnowflake get widgetChannel => _widgetChannelID == null
      ? null
      : GuildChannelSnowflake(client, _widgetChannelID, _id);

  @Field('system_channel_id', nullable: true)
  String get _systemChannelID;

  /// This guild's designated system message text channel, if one has been set.
  TextChannelSnowflake get systemChannel => _systemChannelID == null
      ? null
      : TextChannelSnowflake(client, _systemChannelID, _id);

  /// The time at which the current user joined this guild.
  @Field('joined_at', optional: true)
  DateTime get joinedAt;

  /// True if this guild is considered to be large.
  @Field('large', optional: true)
  bool get isLarge;

  /// The number of members that exist within this guild.
  @Field('member_count', optional: true)
  int get memberCount;

  // TODO: emojis

  @Field('roles')
  List<String> get _roleIDs;

  /// The roles that exist within this guild.
  Iterable<RoleSnowflake> get roles =>
      _roleIDs.map((roleID) => RoleSnowflake(client, roleID, _id));

  @Field('channels', optional: true)
  List<String> get _channelIDs;

  /// The channels that exist within this guild.
  Iterable<GuildChannelSnowflake> get channels => _channelIDs
      .map((channelID) => GuildChannelSnowflake(client, channelID, _id));
}

@SerializableEnum([0, 1, 2, 3, 4])
enum VerificationLevel { none, low, medium, high, veryHigh }

@SerializableEnum([0, 1])
enum MessageNotificationLevel { all, mentions }

@SerializableEnum([0, 1, 2])
enum ExplicitContentFilterLevel { disabled, membersWithoutRoles, allMembers }

@SerializableEnum([0, 1])
enum MfaLevel { none, elevated }
