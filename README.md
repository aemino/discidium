# discidium (unstable)

### Disclaimer
This library is currently in very early stages of development, and thus is
absolutely not ready to be used in production.  Breaking changes will occur
regularly.

Please see [COVERAGE.md](COVERAGE.md) to view the API coverage status.

Intuitive abstractions for the Discord API with an emphasis on performance.

discidium is a progressive, stateless Discord library that builds on the
fundamental concepts introduced by other Discord libraries, while also trying to
introduce new concepts like asynchronous resource stores, database-backed 
caches, and component modularization.

## Usage

A simple usage example:

```dart
import 'package:discidium/discord.dart';

void main() {
  final client = Client('token')..connect();

  client.onMessageCreate.listen((event) async {
    final message = event.message;

    if (message.content == '%ping') {
      print('ponging');
      await message.channel.send('pong!');
    }
  });
}
```
