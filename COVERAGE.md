# Discord API coverage

## Resources

### Fields

- [x] Channel
	- [x] type

- [x] DMChannel
	- [x] recipients

- [x] GroupDMChannel
	- [ ] name
	- [ ] iconHash
	- [x] owner

- [x] GuildChannel
	- [x] guild
	- [x] position
	- [x] permissionOverwrites
	- [x] name
	- [x] parent

- [x] MessageChannel
	- [x] lastMessage
	- [x] lastPinAt

- [x] TextChannel
	- [x] topic
	- [x] isNsfw

- [x] VoiceChannel
	- [x] bitrate
	- [x] userLimit

- [ ] Emoji
	- [ ] name
	- [ ] permissedRoles
	- [ ] creator
	- [ ]	requiresColons
	- [ ] isManaged
	- [ ] isAnimated

- [x] Guild
	- [x] available
	- [x] name
	- [x] iconHash
	- [x] splashHash
	- [x] isMine
	- [x] owner
	- [ ] myPermissions
	- [x] region
	- [x] afkChannel
	- [x] afkTimeout
	- [x] isEmbedEnabled
	- [x] embedChannel
	- [x] verificationLevel
	- [x] messageNotificationLevel
	- [x] explicitContentFilterLevel
	- [x] features
	- [ ] application
	- [x] isWidgetEnabled
	- [x] widgetChannel
	- [x] systemChannel
	- [x] joinedAt
	- [x] isLarge
	- [x] memberCount
	- [ ] emojis
	- [x] roles
	- [x] channels
	- [ ] members
	- [ ] voiceStates
	- [ ] presences

- [x] Member
	- [x] user
	- [x] nickname
	- [x] roles
	- [x] joinedAt
	- [x] isDeaf
	- [x] isMute

- [x] Message
	- [x] channel
	- [x] author
	- [x] content
	- [x] sentAt
	- [x] lastEditedAt
	- [x] isTts
	- [x] isMentioningEveryone
	- [x] mentionedUsers
	- [x] mentionedRoles
	- [ ] attachments
	- [ ] embeds
	- [ ] reactions
	- [ ] nonce
	- [x] isPinned
	- [x] type
	- [ ] activity
	- [ ] application

- [x] Role
	- [x] guild
	- [x] name
	- [x] color
	- [x] isHoisted
	- [x] position
	- [x] permissions
	- [x] isManaged
	- [x] isMentionable

- [x] User
	- [x] name
	- [x] discriminator
	- [x] avatarHash
	- [x] isBot
	- [x] isMfaEnabled
	- [x] isVerified
	- [x] email

- [ ] ClientUser
	- TODO

- [ ] Webhook
	- [ ] guild
	- [ ] channel
	- [ ] user
	- [ ] name
	- [ ] avatarHash
	- [ ] token

### Methods

- TODO

## Structures

- [x] Presence
	- [x] user
	- [x] member
	- [x] activity
	- [x] guild
	- [x] status

- [x] PresenceActivity
	- [x] name
	- [x] type
	- [x] url
	- [x] startedAt
	- [x] endedAt
	- [ ] application
	- [x] details
	- [x] state
	- [ ] party
	- [ ] assets

- [ ] PresenceParty
	- [ ] id
	- [ ] occupancy
	- [ ] size

- [ ] ActivityAssets
	- [ ] primaryImageID
	- [ ] header
	- [ ] secondaryImageID
	- [ ] subtitle

## Events

- [x] Channel
	- [x] create
	- [x] update
	- [x] delete
	- [x] pinsUpdate
	- [x] typingStart

- [ ] Guild
	- [x] create
	- [x] update
	- [x] delete
	- [x] banAdd
	- [x] banRemove
	- [ ] emojisUpdate
	- [ ] integrationsUpdate

- [ ] Member
	- [ ] create
	- [ ] update
	- [ ] delete
	- [ ] chunk

- [ ] Message
	- [x] create
	- [x] update
	- [x] delete
	- [ ] deleteBulk
	- [ ] reactionAdd
	- [ ] reactionRemove
	- [ ] reactionRemoveAll

- [x] Presence
	- [x] update

- [x] Role
	- [x] create
	- [x] update
	- [x] delete

- [x] User
	- [x] update

- [ ] Voice
	- [ ] stateUpdate
	- [ ] serverUpdate

- [ ] Webhook
	- [ ] update
